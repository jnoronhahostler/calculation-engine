#!/bin/env bash
set -e

cd ${CE_BASE_DIR}/app

bash ../run/wait-for-it.sh ${DATABASE_HOST}:${DATABASE_PORT:-5432} --timeout=0
bash ../run/wait-for-it.sh ${MESSAGE_BROKER_HOST}:${MESSAGE_BROKER_PORT:-5672} --timeout=0
bash ../run/wait-for-it.sh ${API_SERVER_HOST}:${API_SERVER_PORT} --timeout=0

if [[ $DEV_MODE == "true" ]]; then
    watchmedo auto-restart --directory=./ --pattern=*.py --recursive -- \
    celery -A calculation_engine beat --loglevel ${CELERY_LOG_LEVEL:-DEBUG}
else
    celery -A calculation_engine beat --loglevel ${CELERY_LOG_LEVEL:-WARNING}
fi
