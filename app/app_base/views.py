from django.contrib.auth.mixins import UserPassesTestMixin
from calculation_engine.views import IsAdmin
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, status
from rest_framework import permissions
from app_base.serializers import UserSerializer, GroupSerializer
from django.views.generic import ListView
from rest_framework.decorators import api_view
from rest_framework import viewsets
# from calculation_engine.models import Job
from django.shortcuts import render
from django.http import HttpResponseForbidden
from rest_framework import generics, views
import os
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist

from calculation_engine.log import get_logger
logger = get_logger(__name__)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [IsAdmin]

    def destroy(self, request, pk=None):
        response = Response()
        username = pk
        try:
            user = User.objects.get(username__exact=username)
            user.delete()
        except ObjectDoesNotExist:
            response.data = f'User "{username}" not found.'
            response.status_code = status.HTTP_404_NOT_FOUND
        return response

    def create(self, request):
        response = Response()
        username = request.data['username']
        user = User.objects.filter(username__exact=username)
        if user:
            response.data = f'User "{username}" already exists.'
            response.status_code = status.HTTP_409_CONFLICT
            return response
        user = User.objects.create(
            username=username,
            email=request.data['email'],
            is_staff=request.data['is_staff'],
            first_name=request.data['first_name'],
            last_name=request.data['last_name']
        )
        user.set_password(request.data['password'])
        user.save()
        return response

class GroupViewSet(UserPassesTestMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [IsAdmin]

    def test_func(self):
        return self.request.user.is_superuser

def HomePageView(request):
    context = {
        'title': 'Calculation Engine'
    }
    return render(request, "calculation_engine/home.html", context)

class UserListView(UserPassesTestMixin, ListView):

    def test_func(self):
        return self.request.user.is_superuser

    def handle_no_permission(self):
        return HttpResponseForbidden(content='You must be authenticated.')

    model = User
    template_name = 'app_base/user_list.html'


class CustomAuthToken(UserPassesTestMixin, ObtainAuthToken):
    permission_classes = [permissions.IsAuthenticated]

    def test_func(self):
        return self.request.user.is_staff

    def get(self, request, format=None):
        token, created = Token.objects.get_or_create(user=request.user)
        return Response({'token': token.key})


@api_view(['POST'])
def get_token(request, format=None):
    response = Response()
    username = request.data['username']
    password = request.data['password']
    # logger.debug(f'Fetching token: username: {username}, password: {password}')
    user_search = User.objects.filter(username__exact=username)
    if not user_search:
        response.status_code = status.HTTP_404_NOT_FOUND
        return response
    user = user_search[0]
    if not user.check_password(password):
        response.status_code = status.HTTP_403_FORBIDDEN
        return response
    token, created = Token.objects.get_or_create(user=user)
    return Response(data={'token': token.key})
