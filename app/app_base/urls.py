from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from app_base import views
from calculation_engine.urls import api_router as ce_api_router, upload_detail, upload_list
from calculation_engine import urls as ce_page_urls
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view

# # Imports for serving static files in development mode
# # ref: https://docs.djangoproject.com/en/4.2/howto/static-files/#serving-static-files-during-development
# from django.conf import settings
# from django.conf.urls.static import static

from calculation_engine.log import get_logger
logger = get_logger(__name__)


router = routers.DefaultRouter()
router.register(r'user', views.UserViewSet)

urlpatterns = [
    path('', views.HomePageView, name='home'),
    path('token', views.CustomAuthToken.as_view(), name='token'),
    path('admin/', admin.site.urls, name='admin'),
    path('oidc/', include('mozilla_django_oidc.urls')),
    path('users/', views.UserListView.as_view(), name='user-list-page'),
    path('ce/', include(ce_page_urls)),
    path('api/v0/', include(router.urls), name='base-api'),
    path('api/v0/token/', views.get_token, name='token-api'),
    path('api/v0/ce/', include(ce_api_router.urls)),
    path('api/v0/ce/upload/', upload_list, name='upload-list'),
    path('api/v0/ce/upload/<str:pk>/', upload_detail, name='upload-detail'),
]
# ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += [
    # Use the `get_schema_view()` helper to add a `SchemaView` to project URLs.
    #   * `title` and `description` parameters are passed to `SchemaGenerator`.
    #   * Provide view name for use with `reverse()`.
    path('openapi/', get_schema_view(
        title="MUSES Calculation Engine API",
        description="API for the MUSES Calculation Engine",
        version="1.0.0",
        patterns=urlpatterns,
        public=True,
        # urlconf='app_base.urls',
    ), name='openapi-schema'),
    # ...
    # Route TemplateView to serve Swagger UI template.
    #   * Provide `extra_context` with view name of `SchemaView`.
    path('swagger-ui/', TemplateView.as_view(
        template_name='calculation_engine/swagger-ui.html',
        extra_context={'schema_url': 'openapi-schema'}
    ), name='swagger-ui'),
]

# Django Silk profiler (https://github.com/jazzband/django-silk)
urlpatterns += [path('silk/', include('silk.urls', namespace='silk'))]
