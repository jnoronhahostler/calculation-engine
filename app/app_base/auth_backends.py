# This auth backend subclass fixes a bug related to the error:
#   OIDC callback state not found in session `oidc_states`!
# ref: https://github.com/mozilla/mozilla-django-oidc/issues/435


from mozilla_django_oidc.auth import OIDCAuthenticationBackend
import unicodedata
from django.conf import settings
import os

from calculation_engine.log import get_logger
logger = get_logger(__name__)

class KeycloakOIDCAuthenticationBackend(OIDCAuthenticationBackend):
    def __init__(self):
        OIDCAuthenticationBackend.__init__(self)

    def create_user(self, claims):
        """ Overrides Authentication Backend so that Django users are
            created with the keycloak preferred_username.
            If nothing found matching the email, then try the username.
        """
        logger.debug(f'KeycloakOIDCAuthenticationBackend.create_user claims: {claims}')
        new_user = self.UserModel.objects.create(
            username=claims.get('preferred_username'),
            email=self.get_email(claims),
            is_staff=is_member_of(claims, settings.STAFF_GROUP),
            is_superuser=is_member_of(claims, settings.ADMIN_GROUP),
            first_name=claims.get('given_name', ''),
            last_name=claims.get('family_name', ''),
        )
        logger.debug(f'''user: {new_user}''')
        return new_user

    def filter_users_by_claims(self, claims):
        """ Return all users matching the specified email.
            If nothing found matching the email, then try the username
        """
        email = claims.get('email')

        if not email:
            return self.UserModel.objects.none()
        users = self.UserModel.objects.filter(email__iexact=email)
        return users

    def update_user(self, user, claims):
        user.first_name = claims.get('given_name', '')
        user.last_name = claims.get('family_name', '')
        user.email = claims.get('email')
        user.is_staff = is_member_of(claims, settings.STAFF_GROUP)
        user.is_superuser=is_member_of(claims, settings.ADMIN_GROUP)
        user.save()
        return user
    

    def get_email(self, claims):
        """Extract email from claims"""
        email = ""
        if "email" in claims:
            email = claims.get("email")
        elif "email_list" in claims:
            email = claims.get("email_list")

        if isinstance(email, list):
            email = email[0]
        return email


def generate_username(email):
    # Using Python 3 and Django 1.11+, usernames can contain alphanumeric
    # (ascii and unicode), _, @, +, . and - characters. So we normalize
    # it and slice at 150 characters.
    return unicodedata.normalize('NFKC', email)[:150]


def execute_logout(request):
    """This implements the OIDC_OP_LOGOUT_URL_METHOD from Django settings. It is
    currently a placeholder.

    NOTES:
      * must return the logout URL
      * called as a hook (via settings.OIDC_OP_LOGOUT_URL_METHOD) from
        mozilla_django_oidc.OIDCLogoutView.post() (from /logout endpoint)
      * the request.user is a django.utils.functional.SimpleLazyObject which is a wrapper
        around a django.contrib.auth.User (see SO:10506766).
    """

    return settings.LOGOUT_REDIRECT_URL

def is_member_of(claims, group):
    return group in claims.get('groups', [])
