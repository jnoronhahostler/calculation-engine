from django.contrib import admin
from calculation_engine.models import Upload


class UploadAdmin(admin.ModelAdmin):
    pass


admin.site.register(Upload, UploadAdmin)
