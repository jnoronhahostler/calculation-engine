from calculation_engine.models import Job, JobFile, Upload, Metrics
from rest_framework import serializers
from calculation_engine.log import get_logger

logger = get_logger(__name__)


class CurrentUserDefault:
    requires_context = True

    def __call__(self, serializer_field):
        logger.debug(serializer_field.context['request'].user)
        return serializer_field.context['request'].user.email

    def __repr__(self):
        return '%s()' % self.__class__.__name__


class JobSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Job
        read_only_fields = ['owner', 'created', 'uuid', 'error_info', 'status', 'modified', 'files']
        fields = read_only_fields + ['name', 'description', 'files', 'saved', 'public', 'config']
    # TODO: Is CurrentUserDefault() what we want to use here?
    owner = CurrentUserDefault()
    files = serializers.SerializerMethodField()

    def get_files(self, job):
        jobfiles = JobFile.objects.filter(job__exact=job)
        return [{'path': jobfile.path, 'size': jobfile.size} for jobfile in jobfiles]


class UploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Upload
        fields = ('file', 'created', 'path',
                  'owner', 'public', 'description', 'uuid', 'size')
        read_only_fields = ['owner', 'created', 'uuid', 'size']


class MetricsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Metrics
        fields = (
            'time_collected',
            'jobs_total',
            'jobs_success',
            'jobs_failure',
            'users_count',
            'users_active',
            'uploads_total',
            'uploads_size',
            'job_files_total',
            'job_files_size',
        )
        read_only_fields = [
            'time_collected',
            'jobs_total',
            'jobs_success',
            'jobs_failure',
            'users_count',
            'users_active',
            'uploads_total',
            'uploads_size',
            'job_files_total',
            'job_files_size',
        ]
