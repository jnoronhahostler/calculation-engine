from rest_framework.response import Response
from django.views.generic import ListView
from django.core.exceptions import ValidationError
from calculation_engine.models import Job, Upload, JobFile, Metrics
from rest_framework import viewsets, status, renderers
from django.http import HttpResponseForbidden
from calculation_engine.serializers import JobSerializer, UploadSerializer
from rest_framework.parsers import FormParser, MultiPartParser
from calculation_engine.tasks import delete_job_files
from silk.profiling.profiler import silk_profile
from rest_framework.permissions import BasePermission
from calculation_engine.log import get_logger
from rest_framework.decorators import action
from calculation_engine.object_store import ObjectStore
from calculation_engine.models import update_job_state
import os
import yaml
from calculation_engine.workflows import Workflow
from django.http import StreamingHttpResponse
from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime, timedelta


logger = get_logger(__name__)

s3 = ObjectStore()


class IsStaff(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_staff


class IsAdmin(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_superuser


def launch_workflow(request_config={}, job_id=''):
    update_job_state(job_id, Job.JobStatus.PENDING)
    wf_config = request_config['workflow']['config']
    logger.debug(f'''Workflow config:\n{yaml.dump(wf_config, indent=2)}''')
    wf = Workflow(config=wf_config, job_id=job_id)
    logger.debug(f'''Workflow object:\n{wf}''')
    logger.debug('''Running workflow...''')
    wf.run_workflow()


class JobViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows jobs to be viewed or edited.
    """
    model = Job
    serializer_class = JobSerializer
    permission_classes = [IsStaff | IsAdmin]

    def get_queryset(self):
        if self.request.user.is_superuser:
            queryset = Job.objects.all()
        else:
            queryset = Job.objects.filter(owner__exact=self.request.user)
        return queryset

    @silk_profile(name='Create Job')
    def create(self, request, *args, **kwargs):
        logger.debug(f'Creating job from request: {request.data}')
        # Create the Job table record
        response = super().create(request, args, kwargs)
        uuid = response.data['uuid']
        job = Job.objects.get(uuid__exact=uuid)
        job.owner = request.user
        job.save()
        # Launch workflow as async Celery tasks
        try:
            request_config = job.config
        except Exception as e:
            logger.error(f'''Error: {e}''')
            response.status_code = status.HTTP_400_BAD_REQUEST
            response.data = f'''{e}'''
            return response
        logger.debug(f'''Launching Celery task for workflow "{job.name}"...''')
        try:
            launch_workflow(request_config=request_config, job_id=uuid)
        except Exception as err:
            err_msg = f'Failed to launch workflow: {err}'
            logger.error(err_msg)
            response.status_code = status.HTTP_400_BAD_REQUEST
            response.data = f'''{err_msg}'''
        return response

    def destroy(self, request, pk=None, *args, **kwargs):
        # TODO: We may want to create new records in a "deleted_job" table for analysis purposes,
        #       or perhaps there is a dedicated "stats" table with hook functions on operations
        #       to track usage.
        # TODO: Revoke Celery task if job is still running. This is not working. Getting errors like:
        #       ERROR/MainProcess] pidbox command error: TypeError('sequence item 0: 
        #       expected str instance, UUID found')
        # instance = self.get_object()
        # task_id = instance.uuid
        # try:
        #     result = AsyncResult(task_id)
        #     result.revoke()
        # except Exception as e:
        #     logger.error(f'''{e}''')
        response = Response()
        job_id = pk
        logger.info(f'''Deleting job "{job_id}"...''')
        # Delete job files
        delete_job_files.delay(job_id)
        # Delete job record from database
        response = super().destroy(request, job_id, args, kwargs)
        return response

    def partial_update(self, request, pk=None, *args, **kwargs):
        response = Response()
        uuid = pk
        try:
            job = self.get_queryset().filter(uuid__exact=uuid)
        except ValidationError:
            response.data = 'Invalid UUID.'
            response.status_code = status.HTTP_400_BAD_REQUEST
            return response
        if not job:
            response.data = f'Job {uuid} not found or not job owner.'
            response.status_code = status.HTTP_404_NOT_FOUND
            return response
        response = super().partial_update(request, uuid, args, kwargs)
        return response


class JobListView(ListView):

    model = Job
    permission_classes = [IsStaff | IsAdmin]
    template_name = 'calculation_engine/job_list.html'

    def handle_no_permission(self):
        return HttpResponseForbidden(content='You must be authenticated.')

    def get_queryset(self):
        if self.request.user.is_superuser:
            queryset = super().get_queryset()
        else:
            queryset = super().get_queryset().filter(owner__exact=self.request.user)
        return queryset


class PassthroughRenderer(renderers.BaseRenderer):
    """
        Return data as-is. View should supply a Response.
    """
    media_type = ''
    format = ''

    def render(self, data, accepted_media_type=None, renderer_context=None):
        return data


class JobFileDownloadViewSet(viewsets.ViewSet):

    permission_classes = [IsStaff | IsAdmin]

    def download(self, request, job_id=None, file_path=None, *args, **kwargs):
        job_id = str(job_id)
        file_path = file_path.strip('/')
        obj_key = os.path.join('/jobs', job_id, file_path)
        file_path = os.path.join('/', file_path)
        response = Response()
        job_file = JobFile.objects.filter(job__owner__exact=self.request.user,
                                          job__uuid__exact=job_id,
                                          path__exact=file_path)
        if not job_file:
            response.data = f'File "{file_path}" not found for job {job_id}.'
            response.status_code = status.HTTP_404_NOT_FOUND
            return response
        job_file = job_file[0]

        obj = s3.get_object(obj_key)
        obj_body = obj['Body']
        logger.debug(obj_body)
        response = StreamingHttpResponse(streaming_content=obj_body.iter_chunks())
        response['Content-Disposition'] = 'attachment; filename="%s"' % os.path.basename(file_path)
        return response


class UploadDownloadViewSet(viewsets.ViewSet):

    permission_classes = [IsStaff | IsAdmin]

    # def get_queryset(self):
    #     if self.request.user.is_superuser:
    #         queryset = Upload.objects.all()
    #     else:
    #         queryset = Upload.objects.filter(owner__exact=self.request.user)
    #     return queryset

    def download(self, request, upload_id=None, *args, **kwargs):
        response = Response(
            status=status.HTTP_200_OK,
        )
        upload_id = str(upload_id)
        uploaded_file = Upload.objects.filter(uuid__exact=upload_id)
        try:
            uploaded_file = Upload.objects.get(uuid__exact=upload_id)
            assert uploaded_file.owner == request.user or uploaded_file.public
        except (ObjectDoesNotExist, AssertionError):
            response = Response(
                status=status.HTTP_404_NOT_FOUND,
                data=f'Upload {upload_id} not found or is private.')
            return response
        # get an open file handle
        file_handle = uploaded_file.file.open()

        # send file
        response = StreamingHttpResponse(streaming_content=file_handle)
        response['Content-Disposition'] = 'attachment; filename="%s"' % uploaded_file.file.name
        return response


class UploadViewSet(viewsets.ModelViewSet):
    parser_classes = (MultiPartParser, FormParser)
    serializer_class = UploadSerializer
    permission_classes = [IsStaff | IsAdmin]
    queryset = Upload.objects.all()
    # def get_queryset(self):
    #     if self.request.user.is_superuser:
    #         queryset = Upload.objects.all()
    #     else:
    #         queryset = Upload.objects.filter(owner__exact=self.request.user)
    #     return queryset

    def destroy(self, request, pk=None):
        response = Response()
        uuid = pk
        if uuid:
            uploaded_file = self.get_queryset().filter(uuid__exact=uuid)
        else:
            response.data = 'uuid must be specified.'
            response.status_code = status.HTTP_400_BAD_REQUEST
            return response
        if not uploaded_file:
            response.data = f'File {uuid} not found.'
            response.status_code = status.HTTP_404_NOT_FOUND
            return response
        uploaded_file = uploaded_file[0]
        logger.info(f'''Deleting uploaded file "{uploaded_file.path}" ("{uploaded_file.uuid}")...''')
        uploaded_file_path = os.path.join('uploads', str(uploaded_file.file))
        logger.debug(uploaded_file_path)
        s3.delete_directory(uploaded_file_path)
        uploaded_file.delete()
        response.data = f'File {uploaded_file.path} deleted.'
        response.status_code = status.HTTP_200_OK
        return response

    def create(self, request):
        response = Response()
        logger.debug(request.data)
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            response.data = serializer.errors
            response.status_code = status.HTTP_400_BAD_REQUEST
            return response
        logger.debug(serializer.validated_data)
        user_path = serializer.validated_data['path']
        uploaded_file = self.get_queryset().filter(path__exact=user_path)
        if uploaded_file:
            uploaded_file = uploaded_file[0]
            response.data = f'File path "{user_path}" already exists with UUID: {uploaded_file.uuid}'
            response.status_code = status.HTTP_409_CONFLICT
            return response
        file_size = s3.object_info(user_path)['ContentLength']
        upload = Upload.objects.create(
            owner=self.request.user,
            public=serializer.validated_data['public'],
            description=serializer.validated_data['description'],
            path=user_path,
            file=serializer.validated_data['file'],
            size=file_size,
        )
        upload.save()
        response.data = {
            'uuid': upload.uuid,
            'path': upload.path,
            'description': upload.description,
            'public': upload.public,
        }
        response.status_code = status.HTTP_201_CREATED
        return response

    def partial_update(self, request, pk=None):
        uuid = pk
        response = Response(
            data=f'No changes made to file {uuid}',
            status=status.HTTP_200_OK,
        )
        logger.debug(f'upload update(): request.data: {request.data}')
        try:
            uploaded_file = self.get_queryset().filter(uuid__exact=uuid)
        except ValidationError:
            response.data = 'Invalid UUID.'
            response.status_code = status.HTTP_400_BAD_REQUEST
            return response
        if not uploaded_file:
            response.data = f'File {uuid} not found.'
            response.status_code = status.HTTP_404_NOT_FOUND
            return response
        uploaded_file = uploaded_file[0]
        if 'public' in request.data:
            uploaded_file.public = request.data['public']
            uploaded_file.save()
            response.data = f'File {uuid} updated.'
        return response

    def list(self, request, *args, **kwargs):
        response = super().list(request, args, kwargs)
        return response

    def retrieve(self, request, pk=None, *args, **kwargs):
        response = Response()
        uuid = pk
        try:
            upload = Upload.objects.get(uuid__exact=uuid)
            assert upload.owner == request.user or upload.public
            # response.data = upload
            response = super().retrieve(request, pk, args, kwargs)
        except (ObjectDoesNotExist, AssertionError):
            response = Response(
                status=status.HTTP_404_NOT_FOUND,
                data=f'Upload {uuid} not found or is private.')
        return response


class UploadListView(ListView):

    model = Upload
    permission_classes = [IsStaff | IsAdmin]
    template_name = 'calculation_engine/upload_list.html'

    def handle_no_permission(self):
        return HttpResponseForbidden(content='You must be authenticated.')

    def get_queryset(self):
        if self.request.user.is_superuser:
            queryset = super().get_queryset()
        else:
            queryset = super().get_queryset().filter(owner__exact=self.request.user)
        return queryset

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, args, kwargs)
        return response

    def delete(self, request, *args, **kwargs):
        response = super().delete(request, args, kwargs)
        return response


class MetricsListView(ListView):

    model = Metrics
    permission_classes = [IsStaff | IsAdmin]
    template_name = 'calculation_engine/metrics.html'

    def get_queryset(self):
        time_threshold = datetime.now() - timedelta(hours=24)
        queryset = super().get_queryset().filter(time_collected__gt=time_threshold)
        for obj in queryset:
            obj.job_files_size_units = 'bytes'
            if obj.job_files_size > 1024**2:
                obj.job_files_size_units = 'MiB'
                obj.job_files_size = round(obj.job_files_size / 1024**2, 0)
            elif obj.job_files_size > 1024**3:
                obj.job_files_size_units = 'GiB'
                obj.job_files_size = round(obj.job_files_size / 1024**3, 0)
        return queryset

    def handle_no_permission(self):
        return HttpResponseForbidden(content='You must be authenticated.')

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, args, kwargs)
        return response
