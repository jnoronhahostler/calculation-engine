import boto3
import botocore
import os
import json
from calculation_engine.log import get_logger
from botocore.exceptions import ClientError
logger = get_logger(__name__)


class ObjectStore:
    def __init__(self) -> None:
        '''Initialize S3 client'''
        self.config = {
            'endpoint-url': os.getenv("S3_ENDPOINT_URL", "http://object-store:9000"),
            'region-name': os.getenv("S3_REGION_NAME", ""),
            'aws_access_key_id': os.getenv("MINIO_ROOT_USER"),
            'aws_secret_access_key': os.getenv("MINIO_ROOT_PASSWORD"),
            'bucket': os.getenv("S3_BUCKET", "calculation-engine"),
        }
        self.client = boto3.client(
            "s3",
            endpoint_url=self.config['endpoint-url'],
            region_name=self.config['region-name'],
            aws_access_key_id=self.config['aws_access_key_id'],
            aws_secret_access_key=self.config['aws_secret_access_key'],
        )
        self.initialize_bucket()

    def initialize_bucket(self):
        '''Initialize bucket'''
        exists = True
        try:
            self.client.head_bucket(Bucket=self.config['bucket'])
        except botocore.exceptions.ClientError as e:
            # If a client error is thrown, then check that it was a 404 error.
            # If it was a 404 error, then the bucket does not exist.
            error_code = e.response["Error"]["Code"]
            if error_code == "404":
                exists = False
        if not exists:
            # Create buckets if they do not exist
            self.client.create_bucket(Bucket=self.config['bucket'])

    def store_folder(self, src_dir="", bucket_root_path=""):
        for dirpath, dirnames, filenames in os.walk(src_dir):
            for filename in filenames:
                self.put_object(
                    path=os.path.join(bucket_root_path, dirpath.replace(src_dir, '').strip('/'), filename),
                    file_path=os.path.join(dirpath, filename),
                )

    def put_object(self, path="", data="", file_path="", json_output=True):
        if data:
            logger.debug(f'''Uploading data object to object store: "{path}"''')
            if json_output:
                body = json.dumps(data, indent=2)
            else:
                body = data
            self.client.put_object(Body=body, Bucket=self.config['bucket'], Key=path)
        elif file_path:
            logger.debug(f'''Uploading file to object store: "{path}"''')
            self.client.upload_file(file_path, self.config['bucket'], path)

    def get_object(self, path=""):
        try:
            obj = self.client.get_object(
                Bucket=self.config['bucket'],
                Key=path)
        except ClientError as ex:
            if ex.response['Error']['Code'] == 'NoSuchKey':
                obj = None
            else:
                raise
        return obj

    def download_object(self, path="", file_path=""):
        self.client.download_file(
            self.config['bucket'],
            path,
            file_path,
        )

    def delete_directory(self, root_path):
        objects = self.client.list_objects(
            Bucket=self.config['bucket'],
            Prefix=root_path,
        )
        if 'Contents' not in objects:
            return
        for obj in objects['Contents']:
            self.client.delete_object(Bucket=self.config['bucket'], Key=obj['Key'])

    def list_directory(self, root_path):
        objects = self.client.list_objects(
            Bucket=self.config['bucket'],
            Prefix=root_path,
        )
        if 'Contents' not in objects:
            return []
        return [obj['Key'] for obj in objects['Contents']]

    def object_info(self, path):
        try:
            response = self.client.head_object(
                Bucket=self.config['bucket'],
                Key=path
            )
        except ClientError as ex:
            if ex.response['Error']['Code'] == 'NoSuchKey':
                response = {}
            else:
                raise
        return response

    def object_exists(self, path):
        if self.object_info(path):
            return True
        else:
            return False

    def copy_directory(self, src_path, dst_root_path):
        objects = self.client.list_objects(
            Bucket=self.config['bucket'],
            Prefix=src_path,
        )
        for obj in objects['Contents']:
            logger.debug(obj)
            dst_rel_path = obj['Key'].replace(src_path, '').strip('/')
            dst_path = os.path.join(dst_root_path, dst_rel_path)
            logger.debug(f'dst_path: {dst_path}')
            self.client.copy_object(CopySource={
                'Bucket': self.config['bucket'],
                'Key': obj['Key']
            }, Bucket=self.config['bucket'], Key=dst_path)
