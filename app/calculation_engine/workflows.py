import yaml
from typing import Dict, List, TypedDict
from celery.canvas import Signature
from calculation_engine.tasks import run_module, workflow_complete, workflow_init
from celery import chain, group

from calculation_engine.log import get_logger
logger = get_logger(__name__)


class Process:
    def __init__(self, conf: dict = {}) -> None:
        self.name: str = conf['name'] if 'name' in conf else ''
        self.module: str = conf['module'] if 'module' in conf else ''
        self.config: dict = conf['config'] if 'config' in conf else {}
        self.pipes: Dict[str, dict] = conf['pipes'] if 'pipes' in conf else {}
        self.inputs: Dict[str, dict] = conf['inputs'] if 'inputs' in conf else {}
        self.task: Signature = None


class Component:
    def __init__(self, conf: dict = {}) -> None:
        self.name: str = conf['name'] if 'name' in conf else ''
        self.type: str = conf['type'] if 'type' in conf else ''
        self.task: Signature = None


class GroupComponent(Component):
    def __init__(self, conf: dict = {}) -> None:
        super().__init__(conf)
        self.group: List[Process] = []


class ChainComponent(Component):
    def __init__(self, conf: dict = {}) -> None:
        super().__init__(conf)
        self.sequence: List[Component] = []


class SplitComponent(Component):
    def __init__(self, conf: dict = {}) -> None:
        super().__init__(conf)
        self.producer: Component = None
        self.consumers: List[Component] = []


class MergeComponent(Component):
    def __init__(self, conf: dict = {}) -> None:
        super().__init__(conf)
        self.producers: List[Component] = []
        self.consumer: Component = None


class Workflow:
    def __init__(self, config: dict = {}, job_id: str = '') -> None:
        self.job_id = job_id
        self.config = config
        self.processes: List[Process] = []
        self.components: List[Component] = []
        err_msg = self.process_config(config)
        if err_msg:
            err_msg = f'Invalid workflow config: {err_msg}'
            logger.error(err_msg)
            raise Exception(err_msg)

    def __str__(self) -> str:
        return yaml.dump({
            'processes': self.processes,
            'components': self.components,
        })

    def find_by_name(self, name):
        process_search = [process for process in self.processes if process.name == name]
        component_search = [component for component in self.components if component.name == name]
        if process_search or component_search:
            # There should only be a single process or component with the name being searched
            return process_search[0] if process_search else component_search[0]
        else:
            return None

    def process_config(self, config: Dict[str, list]) -> None:
        class ProcessConfig(TypedDict):
            name: str
            module: str
            pipes: dict
            inputs: dict
            config: dict

        class ComponentConfig(TypedDict):
            name: str
            type: str

        # Import process definitions
        confs: List[ProcessConfig] = config['processes']
        for conf in confs:
            # Process names must be unique
            name = conf['name']
            assert not self.find_by_name(name)
            # Load default values
            process = Process(conf)
            process.task = run_module.si(
                job_id=self.job_id,
                process_name=process.name,
                module_name=process.module,
                config=process.config,
                pipes=process.pipes,
                inputs=process.inputs,
            )
            self.processes.append(process)
            # logger.debug(self.processes)
        # Import workflow component definitions

        confs: List[ComponentConfig] = config['components']
        for conf in confs:
            name = conf['name']
            # Component must have a unique name
            try:
                assert not self.find_by_name(name)
            except AssertionError:
                err_msg = f'Component name must be unique: "{name}".'
                logger.error(err_msg)
                return err_msg
            component_type = conf['type']
            try:
                assert component_type in ['group', 'chain', 'split', 'merge']
            except AssertionError:
                err_msg = f'Invalid component type: "{component_type}".'
                logger.error(err_msg)
                return err_msg
            if component_type == 'group':
                component = GroupComponent(conf)
                # logger.debug(str(component))
                for item_name in conf['group']:
                    try:
                        item = self.find_by_name(item_name)
                        assert item
                    except AssertionError:
                        err_msg = f'''Component or process not defined: "{item_name}".'''
                        logger.error(err_msg)
                        return err_msg
                    component.group.append(item)
                    # logger.debug(f'component.group: {component.group}')
                component.task = group([item.task for item in component.group])
            elif component_type == 'chain':
                component = ChainComponent(conf)
                for item_name in conf['sequence']:
                    # Sequence items may be names of components or processes
                    component.sequence.append(self.find_by_name(item_name))
                component.task = chain([item.task for item in component.sequence])
            self.components.append(component)
            # logger.debug(self.components)
        return None

    def run_workflow(self) -> None:
        task = self.components[-1].task
        workflow = chain(
            workflow_init.si(job_id=self.job_id, config=self.config),
            task,
            workflow_complete.si(job_id=self.job_id),
        )
        workflow.delay()
