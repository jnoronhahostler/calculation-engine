from calculation_engine import views
from rest_framework import routers
from django.urls import path
from django.contrib.auth.decorators import login_required

api_router = routers.DefaultRouter()
api_router.register(r'job', views.JobViewSet, basename='job')

upload_list = views.UploadViewSet.as_view({
    'get': 'list',
    'put': 'create'
})
upload_detail = views.UploadViewSet.as_view({
    'get': 'retrieve',
    'patch': 'partial_update',
    'delete': 'destroy'
})
jobfile_detail = views.JobFileDownloadViewSet.as_view({
    'get': 'download',
    'post': 'download',
})
urlpatterns = [
    path('jobs/', login_required(views.JobListView.as_view()), name='job-page'),
    path('uploads/', login_required(views.UploadListView.as_view()), name='upload-page'),
    path('metrics/', login_required(views.MetricsListView.as_view()), name='metrics-page'),
    path('download/<uuid:job_id>/<path:file_path>', jobfile_detail, name='download-job-file'),
    path('download/<uuid:upload_id>', views.UploadDownloadViewSet.as_view({
        'get': 'download',
        'post': 'download',
    }), name='download-upload'),
]
