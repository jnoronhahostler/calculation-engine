from celery import shared_task
import os
import time
import json
import yaml
import docker
from celery.result import AsyncResult
from calculation_engine.models import update_job_state
from celery.signals import task_failure
from celery.signals import task_postrun
from celery.exceptions import SoftTimeLimitExceeded
from django.core.exceptions import ObjectDoesNotExist
from calculation_engine.models import Job, Upload, JobFile
from calculation_engine.object_store import ObjectStore
from calculation_engine.log import get_logger
from calculation_engine.config import load_config_from_file
from pathlib import Path
import stat
logger = get_logger(__name__)

s3 = ObjectStore()


task_time_limit = int(os.environ.get("TASK_TIME_LIMIT", "3800"))
task_soft_time_limit = int(os.environ.get("TASK_SOFT_TIME_LIMIT", "3600"))


def validate_inputs(inputs={}, job_id=None):
    '''Validate list of inputs, returning empty string for valid spec or
    the specific error if invalid.'''
    try:
        job = Job.objects.get(uuid__exact=job_id)
        # Get current job object to obtain user info
    except ObjectDoesNotExist:
        return f'Job {job_id} not found.'
    # Iterate over inputs and validate permissions and existence
    for label, input in inputs.items():
        uuid = input['uuid']
        if input['type'] == 'upload':
            # If upload record exists, assume the file is in the storage location
            try:
                upload = Upload.objects.get(uuid__exact=uuid)
                assert upload.owner == job.owner or upload.public
            except (ObjectDoesNotExist, AssertionError):
                return f'Upload {uuid} not found or is private.'
        if input['type'] == 'job':
            # If saved job record exists, verify the file is in the specified location
            try:
                # TODO: Should we require that the referenced job is saved, or
                #       leave as is to allow the files to be purged after expiration?
                # savedjob = Job.objects.get(uuid__exact=uuid, saved__exact=True)
                savedjob = Job.objects.get(uuid__exact=uuid)
                assert savedjob.owner == job.owner or savedjob.public
            except (ObjectDoesNotExist, AssertionError):
                return f'Saved job {uuid} not found or is private.'
            path = input['path']
            job_file_path = os.path.join(
                'jobs',
                uuid,
                path.strip('/'),
            )
            if not s3.object_exists(path=job_file_path):
                return f'Saved job {uuid} file at path {path} not found.'
    return ''


def initialize_io_dir(dir_path=''):
    os.makedirs(dir_path, exist_ok=True)
    # Initialize the path with open permissions so the module container can write files regardless of UID
    os.chmod(dir_path, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
    logger.debug(f'''dir stat for "{dir_path}": {os.stat(dir_path)}''')


def create_mounted_volumes(module_bindings={}, module_name='', config={}):
    # Construct Docker bind mounts for each input/output file
    mounted_volumes = []
    for label, binding in module_bindings.items():
        logger.debug(f'''Binding "{label}":\n{yaml.dump(binding)}''')
        # Mount input files read-only
        # read_only = binding['type'] == 'input'
        read_only = False
        docker_volume = docker.types.Mount(binding['container'], binding['host'], type='bind', read_only=read_only)
        if docker_volume not in mounted_volumes:
            mounted_volumes.append(docker_volume)
        else:
            logger.debug(f'''Redundant volume skipped: "{docker_volume}"''')
    if not module_name or not config:
        return mounted_volumes
    # Construct Docker bind mounts for any explicitly specified volumes
    try:
        explicit_volumes = [mod['volumes'] for mod in config['modules'] if mod['name'] == module_name]
    except KeyError:
        explicit_volumes = []
    if module_name and explicit_volumes:
        for binding in explicit_volumes[0]:
            host_path = binding['host']
            if not os.path.isabs(host_path):
                host_path = os.path.join(config['server']['fileBasePath'], host_path)
            docker_volume = docker.types.Mount(binding['container'], host_path,
                                               type='bind', read_only=binding['readOnly'])
            if docker_volume not in mounted_volumes:
                mounted_volumes.append(docker_volume)
            else:
                logger.debug(f'''Redundant volume skipped: "{docker_volume}"''')
    return mounted_volumes


@shared_task(bind=True, name='Run Module',
             time_limit=task_time_limit,
             soft_time_limit=task_soft_time_limit)
def run_module(
        self,
        job_id='',
        module_name="",
        process_name="",
        config={},
        pipes={},
        inputs={},
        sleep_time=0):
    logs = []
    task_status = {}
    s3_basepath = f'''jobs/{job_id}/{process_name}'''
    try:
        # Validate inputs spec, ensuring that the files exist and that
        # the current job owner has permission to access the files.
        invalid_msg = validate_inputs(inputs=inputs, job_id=job_id)
        assert not invalid_msg
    except AssertionError:
        err_msg = f'Invalid inputs spec: "{invalid_msg}"'
        logger.error(err_msg)
        raise Exception(err_msg)
    try:
        logs.append(f'Job "{job_id}": Celery task "{self.request.id}" triggered for task "{process_name}" running module "{module_name}".')  # noqa E501
        # Artificially extend duration for testing purposes
        time.sleep(sleep_time)
        # Load CE config
        ce_config = load_config_from_file()
        module_config = [mod for mod in ce_config['modules'] if mod['name'] == module_name][0]
        logger.debug(f'module config: {yaml.dump(module_config)}')
        # Construct the container image URL
        image_info = module_config['image']
        if not image_info['registry']:
            image_url = f'''{image_info['repo']}:{image_info['tag']}'''
        else:
            image_url = f'''{image_info['registry']}/{image_info['repo']}:{image_info['tag']}'''
        # Mount IO volumes
        manifest = module_config['manifest']
        bindings = {}
        task_basepath = os.path.join('/scratch', job_id, process_name)
        initialize_io_dir(task_basepath)
        for io in manifest['inputs']:
            io_label = io['label']
            io_host_filepath = os.path.join(task_basepath, 'inputs', io_label)
            io_host_dir = Path(io_host_filepath).parent
            initialize_io_dir(io_host_dir)
            # Initialize container volume binding for the input file
            bindings[io_label] = {
                'type': 'input',
                'host': io_host_filepath,
                'container': io['path'],
            }
            # If input is the config, write the input config data to disk
            if io_label == 'config':
                logger.debug(f'Writing config file "{io_host_filepath}"...')
                with open(io_host_filepath, 'w') as task_config_file:
                    yaml.dump(config, task_config_file)
                # os.chmod(io_host_filepath, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
                with open(io_host_filepath, 'r') as task_config_file:
                    logger.debug(f'Config file contents:\n"{yaml.load(task_config_file, Loader=yaml.FullLoader)}"')
            # If input is not the config, download the file from the object store
            else:
                pipe = [pipes[label] for label in pipes if label == io_label]
                file = [inputs[label] for label in inputs if label == io_label]
                try:
                    assert not (pipe and file)
                except AssertionError:
                    logger.error(f'''Aborting workflow. Both pipe and input file configured for input "{io_label}"''')
                    raise
                if pipe:
                    pipe = pipe[0]
                    logger.debug(f'''pipe: {pipe}''')
                    prev_module_config = [
                        mod for mod in ce_config['modules'] if mod['name'] == pipe['module']][0]
                    # logger.debug(f'prev_module_config: {yaml.dump(prev_module_config)}')
                    piped_output = [out for out in prev_module_config['manifest']
                                    ['outputs'] if out['label'] == pipe['label']][0]
                    # logger.debug(f'piped_output: {piped_output}')
                    piped_output_s3_path = os.path.join(
                        'jobs',
                        job_id,
                        pipe['process'],
                        piped_output['path'].strip('/'),
                    )
                    s3.download_object(
                        path=piped_output_s3_path,
                        file_path=io_host_filepath,
                    )
                elif file:
                    file = file[0]
                    logger.debug(f'''file: {file}''')
                    uuid = file['uuid']
                    if file['type'] == 'upload':
                        upload = Upload.objects.filter(uuid__exact=uuid)
                        try:
                            assert upload
                        except AssertionError:
                            logger.error(f'''Aborting workflow. Unknown file upload: "{uuid}"''')
                            raise
                        upload = upload[0]
                        with upload.file.open(mode='rb') as uploaded_file, open(io_host_filepath, 'wb') as input_file:
                            input_file.write(uploaded_file.read())
                    elif file['type'] == 'job':
                        path = file['path']
                        job_file_path = os.path.join(
                            'jobs',
                            uuid,
                            path.strip('/'),
                        )
                        s3.download_object(
                            path=job_file_path,
                            file_path=io_host_filepath,
                        )
                else:
                    # If neither pipe nor existing file is specified, verify that the input is not required.
                    # If the `required` key is absent, assume the input/output is required.
                    io_required = io['required'] if 'required' in io else True
                    if io_required:
                        err_msg = f'Required input is missing: "{io_label}"'
                        logger.error(err_msg)
                        raise Exception(err_msg)
        for io in manifest['outputs']:
            io_label = io['label']
            container_dir_path = f'''{Path(io['path']).parent}'''
            host_dir_path = os.path.join(task_basepath, 'outputs', container_dir_path.strip('/'))
            initialize_io_dir(host_dir_path)
            bindings[io_label] = {
                'type': 'output',
                'host': host_dir_path,
                'container': container_dir_path,
            }
            if io_label == 'status':
                status_filepath = os.path.join(host_dir_path, Path(io['path']).name)
        mounted_volumes = create_mounted_volumes(module_bindings=bindings)

        # Obtain Docker client and initialize Docker environment
        client = docker.from_env()
        # Name container with the Celery task ID
        container_name = f'''{job_id}-{process_name}-{self.request.id}'''
        container = client.containers.run(
            name=container_name,
            image=f'''{image_url}''',
            detach=True,
            # auto_remove=True,
            mounts=mounted_volumes,
            command=manifest['command'],
        )
        # For debugging purposes, list the current containers
        # TODO: Move this to a periodic task that prunes obsolete containers
        container_list = [container.name for container in client.containers.list(all=True)]
        logger.info(f'''List of containers: {json.dumps(container_list, indent=2)}''')
        # Fetch container logs in real-time
        logger.info(f'''Log for container "{container.name}":''')
        for line in container.logs(stream=True):
            logs.append(line.strip().decode('utf-8'))
            logger.info(logs[-1])
        # Pause execution until container stops
        container.wait()
        container.remove()
        # Fetch the task reported status
        with open(status_filepath, 'r') as status_file:
            task_status = yaml.load(status_file, Loader=yaml.FullLoader)
        logger.debug(f'''\n{yaml.dump(task_status)}''')
        try:
            assert task_status['code'] >= 200 and task_status['code'] < 300
        except AssertionError:
            logger.error(f'''Aborting workflow. Module reports failure:\n{task_status['message']}''')
            raise
    except SoftTimeLimitExceeded:
        logs.append(f'Time limit exceeded. Task "{self.request.id}" terminated for job "{job_id}".')
        task_status = {
            'code': 500,
            'message': 'Time limit exceeded.'
        }
    s3.put_object(data=logs, path=os.path.join(s3_basepath, 'log.json'))
    uploaded_dirs = []
    for label, binding in bindings.items():
        if binding['type'] == 'output' and binding['host'] not in uploaded_dirs:
            s3.store_folder(
                src_dir=binding['host'],
                bucket_root_path=os.path.join(s3_basepath, binding['container'].strip('/')),
            )
            uploaded_dirs.append(binding['host'])
    return task_status


@shared_task(name='Workflow Complete',
             time_limit=task_time_limit,
             soft_time_limit=task_soft_time_limit)
def workflow_complete(job_id=''):
    # Capture the list of output files
    s3_basepath = f'''jobs/{job_id}'''
    paths = s3.list_directory(s3_basepath)
    for path in paths:
        file_size = s3.object_info(path)['ContentLength']
        JobFile.objects.create(
            job=Job.objects.get(uuid__exact=job_id),
            path=path.replace(s3_basepath, '', 1),
            size=file_size,
        )
    # Set workflow status to success
    update_job_state(job_id, Job.JobStatus.SUCCESS)


@shared_task(name='Workflow Init',
             time_limit=task_time_limit,
             soft_time_limit=task_soft_time_limit)
def workflow_init(job_id: str = '', config: dict = {}):
    # Write workflow config to output folder
    s3_basepath = f'''jobs/{job_id}'''
    s3.put_object(data=yaml.dump(config),
                  path=os.path.join(s3_basepath, 'workflow.yaml'),
                  json_output=False)


@shared_task(name='Delete Job Files',
             time_limit=task_time_limit,
             soft_time_limit=task_soft_time_limit)
def delete_job_files(job_id):
    job_dir = os.path.join('jobs', job_id)
    s3.delete_directory(job_dir)


@task_failure.connect()
def task_failed(task_id=None, exception=None, args=None, traceback=None, einfo=None, **kwargs):
    logger.error("from task_failed ==> task_id: " + str(task_id))
    logger.error("from task_failed ==> args: " + str(args))
    logger.error("from task_failed ==> exception: " + str(exception))
    logger.error("from task_failed ==> einfo: " + str(einfo))
    try:
        update_job_state(kwargs['kwargs']['job_id'], Job.JobStatus.FAILURE, error_info=str(einfo))
        logger.info("From task_failed ==> job status updated")
    except KeyError:
        logger.info(f"From task_failed ==> KeyError: {kwargs['kwargs']}")
        pass


@task_postrun.connect()
def task_postrun_notifier(sender=None, task_id=None, task=None, args=None, retval=None, **kwargs):
    logger.info("From task_postrun_notifier ==> sender: " + str(sender))
    result = AsyncResult(task_id)
    logger.info(f"From task_postrun_notifier ==> state: {result.state}")
    logger.info(f"From task_postrun_notifier ==> retval: {retval}")
