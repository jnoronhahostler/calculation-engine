import os
import yaml
from app_base import settings

from calculation_engine.log import get_logger
logger = get_logger(__name__)

def load_config_from_file():
    # Load configuration file
    def load_yaml_file(config_path):
        logger.info(f'''Loading config file: "{config_path}"''')
        with open(config_path, "r") as conf_file:
            config = yaml.load(conf_file, Loader=yaml.FullLoader)
        return config

    # Load secret configuration
    #
    def import_secret_config(in_conf: dict, secret_conf: dict, path=[]):
        conf = dict(in_conf)
        for key in secret_conf:
            # logger.debug(f'''path: {path}, key: {key}''')
            if key in conf:
                if isinstance(conf[key], dict) and isinstance(secret_conf[key], dict):
                    # TODO: Why does path.append(str(key)) fail here? Why is path + [str(key)] necessary?
                    conf[key] = import_secret_config(conf[key], secret_conf[key], path + [str(key)])
                elif conf[key] == secret_conf[key]:
                    pass
                else:
                    logger.warning(f'''Duplicate key "{key}" with conflicting value found in secret config. Overriding initial value with "{secret_conf[key]}"''')
                    conf[key] = secret_conf[key]
            else:
                conf[key] = secret_conf[key]
        return conf

    # If the config has already been processed, load this and return. Otherwise,
    # perform the full loading process.
    if os.path.exists(settings.CE_CONFIG_PROCESSED_PATH):
        return load_yaml_file(settings.CE_CONFIG_PROCESSED_PATH)
    else:
        logger.debug(f'Processed file not found: "{settings.CE_CONFIG_PROCESSED_PATH}".')
        config = load_yaml_file(settings.CE_CONFIG_PATH)

    secret_config = {}
    if os.path.isfile(settings.CE_CONFIG_SECRET_PATH):
        try:
            with open(settings.CE_CONFIG_SECRET_PATH, "r") as conf_file:
                secret_config = yaml.load(conf_file, Loader=yaml.FullLoader)
        except Exception as e:
            logger.error(f'''Error reading secret configuration: {e}''')
    try:
        config = import_secret_config(config, secret_config)
        # logger.debug(f'''conf: {yaml.dump(config, indent=2)}''')
    except Exception as e:
        logger.error(f'''Error importing secret configuration: {e}''')
        raise

    return config
