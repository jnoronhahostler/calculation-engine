from celery import shared_task
from calculation_engine.models import Job, Metrics, Upload, JobFile
from django.contrib.auth.models import User
from app_base import settings
from datetime import datetime, timedelta
from calculation_engine.tasks import delete_job_files
from calculation_engine.log import get_logger
logger = get_logger(__name__)


class PruneJobs():

    @property
    def task_name(self):
        return "Prune jobs"

    @property
    def task_handle(self):
        return self.task_func

    @property
    def task_frequency_seconds(self):
        return settings.CE_JOB_PURGE_INTERVAL

    @property
    def task_initially_enabled(self):
        return True

    def __init__(self, task_func='') -> None:
        self.task_func = task_func

    def run_task(self):
        logger.info(f'Running periodic task "{self.task_name}"...')

        time_threshold = datetime.now() - timedelta(seconds=settings.CE_JOB_EXPIRATION_PERIOD)
        logger.debug(f'''Jobs older than {time_threshold.strftime('%Y/%m/%d %H:%M:%S')} will be deleted.''')
        # Delete jobs older than the expiration period that have a completed status
        expired_jobs = Job.objects.filter(public__exact=False,
                                          created__lt=time_threshold,
                                          status__in=[Job.JobStatus.SUCCESS,
                                                      Job.JobStatus.FAILURE,
                                                      Job.JobStatus.REVOKED])
        logger.info(f'Number of expired jobs scheduled for deletion: {len(expired_jobs)}.')
        # TODO: Need to find and delete orphaned job data
        for job in expired_jobs:
            job_id = str(job.uuid)
            # Delete job files
            delete_job_files.delay(job_id)
            # Delete job records from database
            job.delete()
            logger.info(f'Deleted expired job "{job_id}".')


class CollectMetrics():

    @property
    def task_name(self):
        return "Collect metrics"

    @property
    def task_handle(self):
        return self.task_func

    @property
    def task_frequency_seconds(self):
        return settings.CE_COLLECT_METRICS_INTERVAL

    @property
    def task_initially_enabled(self):
        return True

    def __init__(self, task_func='') -> None:
        self.task_func = task_func

    def run_task(self):
        logger.info(f'Running periodic task "{self.task_name}"...')
        # Query database for jobs created in the last hour
        # TODO: Is it acceptable that this is not guaranteed to be accurate, that it is an undercount?
        time_threshold = datetime.now() - timedelta(seconds=settings.CE_COLLECT_METRICS_INTERVAL)
        logger.debug(f'''Collecting metrics since {time_threshold.strftime('%Y/%m/%d %H:%M:%S')}...''')
        # Count number of recent jobs
        recent_jobs = Job.objects.filter(created__lt=time_threshold)
        # Count total number of users
        all_users = User.objects.filter()
        all_uploads = Upload.objects.filter()
        all_job_files = JobFile.objects.filter()
        # Count number of unique users who ran jobs
        job_owners = []
        for job in recent_jobs:
            if job.owner not in job_owners:
                job_owners.append(job.owner)
        Metrics.objects.create(
            jobs_total=len(recent_jobs),
            jobs_success=len([job for job in recent_jobs if job.status == Job.JobStatus.SUCCESS]),
            jobs_failure=len([job for job in recent_jobs if job.status == Job.JobStatus.FAILURE]),
            users_count=len(all_users),
            users_active=len(job_owners),
            uploads_total=len(all_uploads),
            uploads_size=sum([upload.size for upload in all_uploads]),
            job_files_total=len(all_job_files),
            job_files_size=sum([job_file.size for job_file in all_job_files]),
        )


@shared_task
def prune_jobs():
    PruneJobs().run_task()


@shared_task
def collect_metrics():
    CollectMetrics().run_task()


periodic_tasks = [
    PruneJobs(task_func='prune_jobs'),
    CollectMetrics(task_func='collect_metrics'),
]
