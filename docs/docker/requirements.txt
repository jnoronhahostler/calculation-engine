sphinx
sphinx_rtd_theme
recommonmark
pyyaml
GitPython == 3.1.30
myst_parser
sphinxcontrib-katex
