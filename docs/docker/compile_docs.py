import os
import copy
import yaml
from shutil import copytree
import subprocess
from git import Repo
from urllib.parse import quote_plus
import logging


def get_logger(name):
    # Configure logging
    logging.basicConfig(format='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
    logger = logging.getLogger(name)
    logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))
    return logger


logger = get_logger(__name__)


def get_clone_dir(url, rev):
    # URL-encode the git URL and revision suitable for a POSIX file path
    return os.path.join('/tmp', 'modules', quote_plus(url.strip('/').strip('.git')), quote_plus(rev))


def clone_git_repo(url, rev="", repo_dir=""):
    if not repo_dir:
        repo_dir = get_clone_dir(url, rev)
    if not os.path.isdir(repo_dir):
        # Hack to clone private repos from GitLab.com
        gitlab_user = os.getenv('GITLAB_USER', '')
        logger.debug(f' GitLab user: "{gitlab_user}"')
        gitlab_pass = os.getenv('GITLAB_TOKEN', '')
        if gitlab_user and gitlab_pass:
            url = url.replace('https://gitlab.com', f'https://{gitlab_user}:{gitlab_pass}@gitlab.com')
            logger.info(f'Using GitLab user "{gitlab_user}" credentials...')
        repo = Repo.clone_from(url, repo_dir)
        # A revision must be specified
        assert rev
        repo.git.checkout(rev)
    return repo_dir


def main():
    config_path = os.path.join(os.environ.get('HOME'), 'config', 'config.yaml')
    logger.info(f'''Loading config file: "{config_path}"''')
    with open(config_path, "r") as fp:
        config = yaml.load(fp, Loader=yaml.FullLoader)
    modules = []
    for module in config['modules']:
        logger.debug(f'''Loading "{module['name']}"...''')
        try:
            url = module['url']
            assert url
            assert module['targetRevision']
            targetRevision = module['targetRevision']
            path = module['path']
            # Clone module git repo
            logger.debug(f'''Cloning "{module['name']}"...''')
            module['dir'] = clone_git_repo(url=url, rev=targetRevision)
            module['manifest_path'] = os.path.join(module['dir'], path, 'manifest.yaml')
            modules.append(copy.deepcopy(module))
        except Exception as err:
            # Failure to copy source code for any module is a fatal error
            logger.fatal(f'''Error copying module source code: {err}:''')
            raise
    for module in modules:
        # with open(module['manifest_path']) as fp:
        #     manifest = yaml.load(fp, Loader=yaml.FullLoader)
        # docs_path = os.path.join(module['dir'], manifest['docs']['path'])
        docs_path = os.path.join(module['dir'], module['manifest']['docs']['path'])
        logger.debug(docs_path)
        module_docs_path = os.path.join(os.environ.get('HOME'), 'docs/modules', module['name'])
        copytree(docs_path, module_docs_path, dirs_exist_ok=True)

    os.chdir(os.path.join(os.environ.get('HOME'), 'docs'))
    proc = subprocess.Popen(['make', 'clean'])
    try:
        outs, errs = proc.communicate(timeout=30)
    except Exception as err:
        proc.kill()
        outs, errs = proc.communicate()
        logger.error(f'''Sphinx clean error:\n{err}\n{outs}\n{errs}''')
        raise
    proc = subprocess.Popen(['make', 'html'])
    try:
        outs, errs = proc.communicate(timeout=30)
    except Exception as err:
        proc.kill()
        outs, errs = proc.communicate()
        logger.error(f'''Sphinx build error:\n{err}\n{outs}\n{errs}''')
        raise


if __name__ == "__main__":
    main()
