# Nextcloud

## Initial configuration and setup

Initially deploy Nextcloud with a single replica. Otherwise there seems to be errors related to conflict between the replicas as they upgrading from the version of Nextcloud included in the Helm chart to the version specified in the chart release. After Nextcloud is installed and operational (which takes several minutes for Nextcloud to complete its initial setup), there are several manual actions to take as the admin.

## Email configuration

Configure the SMTP settings at https://cloud.musesframework.io/settings/admin using::

- From : devnull@ncsa.illinois.edu
- Server address : smtp.ncsa.uiuc.edu
- Authentication method : None
- Encryption : None

## Other settings

In Activity settings enable Mail notifications for everything by default (https://cloud.musesframework.io/settings/user/activity)

## Backups

TODO: update with the latest backup solution
