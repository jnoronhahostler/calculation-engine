API Server
===========================================

The API server is a Django-based webserver written in Python that implements our API endpoints.

Authentication and authorization
--------------------------------

The API server provides both web resources like webpages and programmatic API functions.

Web resources that require access control use our Keycloak service for single-sign-on (SSO) authentication via OpenID Connect (OIDC).

Authorization utilizes the [Keycloak groups system](https://wjw465150.gitbooks.io/keycloak-documentation/content/server_admin/topics/groups.html), where users can be added to groups for access control purposes. By creating [Keycloak mappers](https://wjw465150.gitbooks.io/keycloak-documentation/content/server_admin/topics/clients/protocol-mappers.html), this group membership information is supplied to our OIDC client applications via the returned OAuth token, allowing them to implement authorization as needed. For example, the API server only allows users in specific groups to access protected content.

Programmatic API endpoints are protected by requiring a standard `Authorization: Bearer token` HTTP request header. These protected endpoints are decorated with the `@authenticated` function. The access token is obtained by accessing the protected URL `/api/v0/token`, which returns a JSON-formatted data structure containing a [Java Web Token (JWT)](https://jwt.io/). The API server signs each generated JWT using a secret that protects the tokens from forgery. These tokens also have a TTL, or expiration period, after which they become invalid. For programmatic use of extended duration, users may use the `/api/v0/token` endpoint to exchange a valid token for a new one prior to expiration.
