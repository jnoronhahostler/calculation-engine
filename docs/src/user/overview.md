# Overview

This section of the documentation includes topics of interest to end users of the MUSES services.

A wealth of documentation and information about MUSES services and how to use them is found on the [MUSES Forum](https://forum.musesframework.io). There you can use the search tool to find answers and also post your own questions to seek help from the MUSES community.

## Links

- [MUSES accounts](https://forum.musesframework.io/t/muses-accounts)
- [Collaborator onboarding](https://forum.musesframework.io/t/collaborator-onboarding)
- [List of web services](https://forum.musesframework.io/t/web-services)
