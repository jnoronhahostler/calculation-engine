# Backup and restore

## Backups

Most applications backups are performed by our [`backups` Helm chart](https://gitlab.com/decentci/charts/-/tree/main/charts/backups), which makes backups of data files as well as SQL databases.

## Example restore procedure

Follow the [instructions in the `backups` Helm chart `Readme.md`](https://gitlab.com/decentci/charts/-/tree/main/charts/backups#restore) to restore from backups of the files and the database.

```bash
$ export KUBECONFIG=/home/manninga/.kube/cluster.kubeconfig 

## Scale down discourse app deployment
$ kubectl scale -n discourse deployment discourse --replicas=0
deployment.apps/discourse scaled

## Open a terminal in the restore container
$ kubectl exec -it -n discourse discourse-restore-74959d6c7f-z4vjz -- bash

## Compare the backup `/backup` with the live data files `/restore`
root@discourse-restore-74959d6c7f-z4vjz:/# diff -rq /backup/discourse/snapshot.0/snapshots/data/ /restore/files/data/
Only in /restore/files/data/discourse/public/uploads/default/optimized/1X: b06976de4d67166f039ffe4515e4af5859369cd3_2_10x10.png
Only in /restore/files/data/discourse/public/uploads/default/optimized/1X: b06976de4d67166f039ffe4515e4af5859369cd3_2_499x500.jpeg
Only in /restore/files/data/discourse/public/uploads/default/original/1X: b06976de4d67166f039ffe4515e4af5859369cd3.jpeg

## Sync the files from the backup
root@discourse-restore-74959d6c7f-z4vjz:/# rsync -van --delete /backup/discourse/snapshot.0/snapshots/data/ /restore/files/data/
sending incremental file list
deleting discourse/public/uploads/default/optimized/1X/b06976de4d67166f039ffe4515e4af5859369cd3_2_499x500.jpeg
deleting discourse/public/uploads/default/optimized/1X/b06976de4d67166f039ffe4515e4af5859369cd3_2_10x10.png
deleting discourse/public/uploads/default/original/1X/b06976de4d67166f039ffe4515e4af5859369cd3.jpeg
discourse/public/uploads/default/optimized/1X/
discourse/public/uploads/default/original/1X/

sent 42,355 bytes  received 527 bytes  9,529.33 bytes/sec
total size is 19,204,748  speedup is 447.85 (DRY RUN)

## Restore the database
root@discourse-restore-74959d6c7f-z4vjz:/# export PGPASSWORD="${DB_PASS}"
root@discourse-restore-74959d6c7f-z4vjz:/# pg_restore --verbose --clean --no-acl --no-owner --host=$DB_HOST     --dbname=$DB_NAME --username=$DB_USER < /backup/$DB_HOST/20220419/bitnami_application.20220419215043.dump 
```

Sync the ArgoCD app and verify that the site has been restored.
