# Nextcloud

## Running the `occ` CLI as `www-data` user

To run `occ` as the `www-data` user in the Nextcloud container:

```bash
$ POD_NAME=$(kubectl get pod -n nextcloud --selector=app.kubernetes.io/name=nextcloud -o jsonpath='{.items[0].metadata.name}')
$ kubectl exec -n nextcloud -it $POD_NAME -- bash

root@nextcloud-6d465f97c-f7tg5:/var/www/html# 
root@nextcloud-7d75b866f5-n5rnt:/var/www/html# runuser --user www-data -- bash

www-data@nextcloud-7d75b866f5-n5rnt:~/html$ php occ list

```

## Transfer ownership of files and calendars between user accounts

Sometimes a person changes institutions and needs to log in via a new identity provider. This generates a new Keycloak user account with a new ID. Transfer the old account's files and calendars to the new account using the example below:

```bash
www-data@nextcloud-6d465f97c-f7tg5:~/html$ ./occ files:transfer-ownership keycloak-old-user  keycloak-new-user
Validating quota
Analysing files of keycloak-old-user ...
   28 [============================]
Collecting all share information for files and folders of keycloak-old-user ...
    0 [-------------->-------------]
Transferring files to keycloak-other-user/files/transferred from Jane Doe          on 2022-new-user
Restoring shares ...
    0 [>---------------------------]

www-data@nextcloud-6d465f97c-f7tg5:~/html$ php occ dav:list-calendars keycloak-old-user
+-------------------+-----------------+--------------------------------------------------+------------------------+----------+
| uri               | displayname     | owner's userid                                   | owner's displayname    | writable |
+-------------------+-----------------+--------------------------------------------------+------------------------+----------+
| personal          | Personal        | principals/users/keycloak-old-user | Jane Doe    |  ✓                     |
| muses-seminar-1   | MUSES Seminar   | principals/users/keycloak-old-user | Jane Doe    |  ✓                     |
+-------------------+-----------------+--------------------------------------------------+------------------------+----------+

www-data@nextcloud-6d465f97c-f7tg5:~/html$ php occ dav:list-calendars keycloak-new-user
+-------------------+-----------------+--------------------------------------------------+------------------------+----------+
| uri               | displayname     | owner's userid                                   | owner's displayname    | writable |
+-------------------+-----------------+--------------------------------------------------+------------------------+----------+
| personal          | Personal        | principals/users/keycloak-other-user new-user    |
+-------------------+-----------------+--------------------------------------------------+------------------------+----------+

www-data@nextcloud-6d465f97c-f7tg5:~/html$ php occ dav:move-calendar muses-seminar-1 keycloak-old-user keycloak-new-user

 ! [NOTE] Please note that moving calendar muses-seminar-1 from user <keycloak-old-user> to 
 !        <keycloak-other-usernew-user> has caused share links to change.       
 !                                                                                                                      
 !        Sharees will need to change                                                                                   
 !        "example.com/remote.php/dav/calendars/uid/muses-seminar-1_shared_by_keycloak-some-user"
 !         to "example.com/remote.php/dav/calendars/uid/                                                            

 [OK] Calendar <muses-seminar-1> was moved from user <keycloak-old-user> to                 
      <keycloak-new-user>

www-data@nextcloud-6d465f97c-f7tg5:~/html$ php occ dav:list-calendars keycloak-new-user
+-------------------+-----------------+--------------------------------------------------+------------------------+----------+
| uri               | displayname     | owner's userid                                   | owner's displayname    | writable |
+-------------------+-----------------+--------------------------------------------------+------------------------+----------+
| muses-seminar-1   | MUSES Seminar   | principals/users/keycloak-other-user Jane Doe    |
| personal          | Personal        | principals/users/keycloak-other-user Jane Doe    |
+-------------------+-----------------+--------------------------------------------------+------------------------+----------+
```
