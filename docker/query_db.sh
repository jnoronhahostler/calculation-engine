
if [[ $1 == "interactive" ]]; then
docker compose $COMPOSE_ARGS exec -it db bash -c \
    'PG_PASSWORD=$POSTGRES_PASSWORD psql --user=$POSTGRES_USER --dbname=$POSTGRES_DB'
fi
if [[ $1 == "show_jobs" ]]; then
docker compose $COMPOSE_ARGS exec -it db bash -c \
    'PG_PASSWORD=$POSTGRES_PASSWORD psql --user=$POSTGRES_USER --dbname=$POSTGRES_DB --expanded'" -c \" \
    select * from calculation_engine_job LIMIT 5; \
    \""
fi
if [[ $1 == "incomplete" ]]; then
docker compose $COMPOSE_ARGS exec -it db bash -c \
    'PG_PASSWORD=$POSTGRES_PASSWORD psql --user=$POSTGRES_USER --dbname=$POSTGRES_DB'" -c \" \
    select created,modified,uuid,status from calculation_engine_job where status='PENDING' or status='STARTED' order by created desc LIMIT 100; \
    \""
fi
if [[ $1 == "complete" ]]; then
docker compose $COMPOSE_ARGS exec -it db bash -c \
    'PG_PASSWORD=$POSTGRES_PASSWORD psql --user=$POSTGRES_USER --dbname=$POSTGRES_DB'" -c \" \
    select created,modified,uuid,status from calculation_engine_job where status!='PENDING' AND status!='STARTED' order by created desc LIMIT 100; \
    \""
fi
if [[ $1 == "first" ]]; then
docker compose $COMPOSE_ARGS exec -it db bash -c \
    'PG_PASSWORD=$POSTGRES_PASSWORD psql --user=$POSTGRES_USER --dbname=$POSTGRES_DB'" -c \" \
    select created,modified,uuid,status from calculation_engine_job where status!='PENDING' AND status!='STARTED' order by created asc LIMIT 1; \
    \""
fi
if [[ $1 == "last" ]]; then
docker compose $COMPOSE_ARGS exec -it db bash -c \
    'PG_PASSWORD=$POSTGRES_PASSWORD psql --user=$POSTGRES_USER --dbname=$POSTGRES_DB'" -c \" \
    select created,modified,uuid,status from calculation_engine_job where status!='PENDING' AND status!='STARTED' order by created desc LIMIT 1; \
    \""
fi

if [[ $1 == "purge_jobs" ]]; then
docker compose $COMPOSE_ARGS exec -it db bash -c \
    'PG_PASSWORD=$POSTGRES_PASSWORD psql --user=$POSTGRES_USER --dbname=$POSTGRES_DB'" -c \" \
    DELETE from calculation_engine_job; \
    \""
fi
