name: ce

networks:
  internal:
    external: false
  traefik_proxy:
    external: true

volumes:
  postgres: {}
  static: {}
  object-data: {}
  docker-shared: {}
  docker-data: {}
  ce-modules: {}
  ce-scratch: {}

services:

  setup-permissions:
    profiles: ["prod", "full_dev", "slim_dev", ""]
    image: ubuntu:22.04
    container_name: ce-setup
    command:
      - bash
      - -c
      - chown -R ${USERID}:${USERID} /static /scratch /modules
    volumes:
      - static:/static
      - ce-scratch:/scratch
      - ce-modules:/modules

  api-server:
    profiles: ["prod", "full_dev", "slim_dev", ""]
    image: registry.gitlab.com/nsf-muses/calculation-engine:dev
    build:
      context: ..
      dockerfile: docker/Dockerfile
      args:
        CE_BASE_DIR: "${CE_BASE_DIR}"
        UID: "${USERID}"
    deploy:
      mode: replicated
      replicas: 1
    env_file:
      - .env.default
      - .env.dev
    depends_on:
      - setup-permissions
    networks:
      - internal
    command:
      - ${CE_BASE_DIR}/run/run_api_server.sh
    volumes:
      - static:${CE_BASE_DIR}/static
      - ce-modules:${CE_BASE_DIR}/modules
      - ../config:${CE_BASE_DIR}/config
      - ../app:${CE_BASE_DIR}/app
      - ../run:${CE_BASE_DIR}/run
      - ../modules:/modules:ro

  api-proxy:
    profiles: ["prod", "full_dev", "slim_dev", ""]
    image: nginx:1.26
    container_name: api-proxy
    env_file:
      - .env.default
      - .env.dev
    depends_on:
      - api-server
      - flower
      # - s3-gateway
    networks:
      - internal
      - traefik_proxy
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_proxy"
      - "traefik.http.routers.alpha-muses-ncsa-illinois-edu.rule=Host(`alpha.muses.ncsa.illinois.edu`)"
      - "traefik.http.routers.alpha-muses-ncsa-illinois-edu.entrypoints=web,websecure"
      - "traefik.http.routers.alpha-muses-ncsa-illinois-edu.tls.certresolver=leresolver"
      - "traefik.http.services.alpha-muses-ncsa-illinois-edu.loadbalancer.server.port=${API_PROXY_PORT}"
    ports:
      - 127.0.0.1:${API_PROXY_PORT}:${API_PROXY_PORT}
    volumes:
      - ./nginx.conf.tpl:/etc/nginx/nginx.conf.tpl:ro
      - ./nginx_init.sh:/docker-entrypoint.d/nginx_init.sh
      - static:/static



  db:
    profiles: ["prod", "full_dev", "slim_dev", ""]
    image: postgres:16
    container_name: db
    env_file:
      - .env.default
      - .env.dev
    ports:
      - 127.0.0.1:${DATABASE_PORT}:${DATABASE_PORT}
    networks:
      - internal
    volumes:
      - postgres:/var/lib/postgresql

  dind-daemon:
    profiles: ["prod", "full_dev"]
    privileged: true
    image: docker:dind
    command: ["-G", "${USERID}"]
    container_name: dind-daemon
    environment:
      - DOCKER_HOST=unix:///shared/docker.sock
    volumes:
      - docker-shared:/shared
      - docker-data:/var/lib/docker
      - ce-scratch:/scratch

  celery-worker:
    profiles: ["prod", "full_dev"]
    restart: on-failure
    depends_on:
      rabbitmq:
        condition: service_healthy
    image: registry.gitlab.com/nsf-muses/calculation-engine:dev
    build:
      context: ..
      dockerfile: docker/Dockerfile
      args:
        CE_BASE_DIR: "${CE_BASE_DIR}"
    command:
      - ${CE_BASE_DIR}/run/run_celery_worker.sh
    deploy:
      mode: replicated
      replicas: ${CELERY_WORKER_REPLICAS}
    env_file:
      - .env.default
      - .env.dev
    networks:
      - internal
    environment:
      - DOCKER_HOST=unix:///shared/docker.sock
    volumes:
      - ../app:${CE_BASE_DIR}/app
      - docker-shared:/shared
      - docker-data:/var/lib/docker
      - ce-scratch:/scratch
      - ../config:${CE_BASE_DIR}/config
      # - ./docker/config:${CE_BASE_DIR}/config

  celery-beat:
    profiles: ["prod", "full_dev"]
    restart: on-failure
    depends_on:
      rabbitmq:
        condition: service_healthy
    image: registry.gitlab.com/nsf-muses/calculation-engine:dev
    build:
      context: ..
      dockerfile: docker/Dockerfile
      args:
        CE_BASE_DIR: "${CE_BASE_DIR}"
    command:
      - ${CE_BASE_DIR}/run/run_celery_beat.sh
    env_file:
      - .env.default
      - .env.dev
    networks:
      - internal
    volumes:
      - ../app:${CE_BASE_DIR}/app
      - ce-scratch:/scratch
      - ../config:${CE_BASE_DIR}/config
      # - ./docker/config:${CE_BASE_DIR}/config

  rabbitmq:
    profiles: ["prod", "full_dev"]
    image: "rabbitmq:3-management"
    container_name: "rabbitmq"
    env_file:
      - .env.default
      - .env.dev
    ports:
      - 127.0.0.1:${MESSAGE_BROKER_PORT}:${MESSAGE_BROKER_PORT}
      - 127.0.0.1:${MESSAGE_BROKER_MANAGEMENT_PORT}:${MESSAGE_BROKER_MANAGEMENT_PORT}
    networks:
      - internal
    healthcheck:
      test: [ "CMD", "rabbitmqctl", "status"]
      # test: [ "CMD", "nc", "-z", "rabbitmq", "5672" ]
      interval: 5s
      timeout: 20s
      retries: 5

  flower:
    profiles: ["prod", "full_dev", "slim_dev"]
    restart: on-failure
    depends_on:
      - api-server
      # - rabbitmq
      # - celery-worker
    image: registry.gitlab.com/nsf-muses/calculation-engine:dev
    build:
      context: ..
      dockerfile: docker/Dockerfile
      args:
        CE_BASE_DIR: "${CE_BASE_DIR}"
    container_name: flower
    command:
      - ${CE_BASE_DIR}/run/run_flower.sh
    ports:
      - 127.0.0.1:${FLOWER_PORT}:${FLOWER_PORT}
    env_file:
      - .env.default
      - .env.dev
    networks:
      - internal

  ## 
  ## S3-compatible object store for archived message data
  ##
  object-store:
    profiles: ["prod", "full_dev", "slim_dev", ""]
    image: quay.io/minio/minio:RELEASE.2024-04-28T17-53-50Z
    container_name: object-store
    env_file:
      - .env.default
      - .env.dev
    ports:
      - 127.0.0.1:${S3_CONSOLE_PORT}:${S3_CONSOLE_PORT}
      - 127.0.0.1:${S3_SERVER_PORT}:${S3_SERVER_PORT}
    networks:
      - internal
    command:
    - server
    - /data
    - --console-address
    - ":${S3_CONSOLE_PORT}"
    volumes:
      - object-data:/data

  s3-gateway:
    profiles: ["full_dev", "slim_dev", ""]
    image: ghcr.io/nginxinc/nginx-s3-gateway/nginx-oss-s3-gateway:latest-20240113
    container_name: s3-gateway
    env_file:
      - .env.default
      - .env.dev
    ports:
      - 127.0.0.1:${S3_GATEWAY_PORT}:80
    networks:
      - internal
