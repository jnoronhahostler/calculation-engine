#!/bin/env bash
set -e

export DOLLAR="$"
envsubst < /etc/nginx/nginx.conf.tpl > /etc/nginx/nginx.conf
cat /etc/nginx/nginx.conf
