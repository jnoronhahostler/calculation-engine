import sys
import logging
import time
from typing import Dict
from calculation_engine_api import CalculationEngineApi
from calculation_engine_api import run_and_monitor_workflow
from calculation_engine_api import get_staff_apis
from calculation_engine_api import purge_and_create_users
import os
import yaml
from pathlib import Path
logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
logger = logging.getLogger(__name__)


def test_shared_saved_job(apis: Dict[str, CalculationEngineApi]) -> None:
    usernames = [username for username in apis]
    username = usernames[0]
    api = apis[username]
    logger.info(f'\nActing as user "{username}":\n')

    logger.info(f'\nRun the test workflow "template_module_1" for "{username}":\n')
    with open(os.path.join(Path(__file__).resolve().parent, 'test_workflow_config', 'test_saved_job_share.1.yaml')) as fp:
        wf_config = yaml.load(fp, Loader=yaml.SafeLoader)
    logger.debug(f'wf_config: {yaml.dump(wf_config)}')
    logger.info('Running job...')
    job_response = run_and_monitor_workflow(
        api=api, config=wf_config,
        loops=10, sleep=10)
    job_id = job_response['uuid']
    api.update_job(job_id=job_id, saved=True, public=True)
    upstream_job_id = job_id

    def run_second_job():
        username = usernames[1]
        api = apis[username]
        logger.info(f'\nActing as user "{username}":\n')
        logger.info(f'\nRun the test workflow "template_module_2" for "{username}" using '
                    'the output from the "template_module_1" job as input:\n')
        with open(os.path.join(Path(__file__).resolve().parent, 'test_workflow_config', 'test_saved_job_share.2.yaml')) as fp:
            wf_config = yaml.load(fp, Loader=yaml.SafeLoader)
        wf_config['processes'][0]['inputs']['eos']['uuid'] = upstream_job_id
        logger.debug(f'wf_config: {yaml.dump(wf_config)}')
        logger.info('Running job...')
        job_response = run_and_monitor_workflow(
            api=api, config=wf_config,
            loops=10, sleep=10)
        job_id = job_response['uuid']
        return job_id

    job_id = run_second_job()

    username = usernames[1]
    api = apis[username]
    logger.info(f'\nActing as user "{username}":\n')
    logger.info('Try to change the original job. This should fail because this user'
                'is not the owner.')
    api.update_job(job_id=upstream_job_id, saved=True, public=False)

    username = usernames[0]
    api = apis[username]
    logger.info(f'\nActing as user "{username}":\n')
    logger.info('Set the original job to private.')
    api.update_job(job_id=upstream_job_id, saved=True, public=False)

    job_id = run_second_job()


if __name__ == "__main__":
    purge_and_create_users()
    staff_apis = get_staff_apis()
    test_shared_saved_job(apis=staff_apis)
