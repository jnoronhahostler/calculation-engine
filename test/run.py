import os
# import yaml
if __name__ == "__main__":
    print('Running generic module...\n')
    status = {
        'code': 200,
        'message': 'All is well that ends well',
    }
    status_text = 'code: 200\nmessage: "All is well that ends well"'
    with open('/tmp/status.yaml', 'w') as fp:
        fp.write(status_text)
    # with open('/input/config.yaml', 'r') as fp:
    #     config = yaml.load(fp, Loader=yaml.SafeLoader)
    data_file_size = 3 * 1024 * 1024
    with open('/tmp/data.dat', 'wb') as fp:
        fp.write(os.urandom(data_file_size))
