import requests
import random
from requests.auth import HTTPBasicAuth
import os
import sys
import json
import logging
from typing import Dict

logging.basicConfig(
    level=os.getenv('LOG_LEVEL', logging.DEBUG),
    stream=sys.stdout)
logger = logging.getLogger(__name__)


class Users():
    staff = {
        'foo': 'password',
        'bar': 'password',
    }


class CalculationEngineApi():
    def __init__(self, conf={}):
        # Import credentials and config from environment variables
        self.conf = {
            'username': os.environ.get('CE_USERNAME', 'admin'),
            'password': os.environ.get('CE_PASSWORD', 'password'),
            'token': os.environ.get('CE_API_TOKEN', ''),
            'api_url_protocol': os.environ.get('CE_API_URL_PROTOCOL', 'http'),
            'api_url_authority': os.environ.get('CE_API_URL_AUTHORITY', 'localhost:4000'),
            'api_url_basepath': os.environ.get('CE_API_URL_BASEPATH', 'api/v0'),
        }
        for param in self.conf:
            if param in conf:
                self.conf[param] = conf[param]
        self.conf['url_base'] = f'''{self.conf['api_url_protocol']}://{self.conf['api_url_authority']}'''
        self.conf['api_url_base'] = f'''{self.conf['url_base']}/{self.conf['api_url_basepath']}'''
        if self.conf['token']:
            self.api_token = self.conf['token']
        else:
            self.api_token = self.get_api_token()
        self.auth_header = {
            'Authorization': f'''Token {self.api_token}''',
        }
        self.json_headers = self.auth_header
        self.json_headers['Content-Type'] = 'application/json'

    def get_api_token(self):
        url = f'''{self.conf['api_url_base']}/token/'''
        response = requests.post(
            url,
            headers={
                'Content-Type': 'application/json',
            },
            json={
                'username': self.conf['username'],
                'password': self.conf['password'],
            }
        )
        response = self.display_response(response)
        return response['token']

    def display_response(self, response, parse_json=True):
        try:
            assert isinstance(response, requests.Response)
            # assert response.text
            # assert response.status_code
        except Exception as err:
            logger.debug(f'''Invalid response object: {err}''')
            return
        logger.debug(f'''Response code: {response.status_code}''')
        try:
            assert response.status_code in range(200, 300)
            if parse_json:
                data = response.json()
                logger.debug(json.dumps(data, indent=2))
                return data
            else:
                return response
        except Exception:
            try:
                logger.debug(
                    f'''ERROR: {json.dumps(response.text, indent=2)}''')
            except Exception:
                logger.debug(f'''ERROR: {response.text}''')
            return response

    def login(self):
        response = requests.post(
            f'''{self.conf['url_base']}/admin/login/''',
            auth=HTTPBasicAuth(self.conf['username'], self.conf['password']),
        )
        data = self.display_response(response)
        return data

    def job_create(self, name='', description="", config={}):
        if not name:
            name = f'''test-{random.randrange(10000,99999)}'''
        response = requests.post(
            # The URL trailing slash is required without setting APPEND_SLASH=False
            f'''{self.conf['api_url_base']}/ce/job/''',
            json={
                'name': name,
                'config': config,
                'description': description,
            },
            headers=self.json_headers,
            # auth=self.basic_auth,
        )
        return self.display_response(response)

    def job_delete(self, uuid=None):
        response = requests.delete(
            # The URL trailing slash is required without setting APPEND_SLASH=False
            f'''{self.conf['api_url_base']}/ce/job/{uuid}/''',
            headers=self.json_headers,
        )
        return self.display_response(response, parse_json=False)

    def job_delete_all(self):
        all_jobs = self.job_list()
        for job in all_jobs['results']:
            logger.debug(job)
            response = requests.delete(
                # The URL trailing slash is required without setting APPEND_SLASH=False
                f'''{self.conf['api_url_base']}/ce/job/{job['uuid']}/''',
                headers=self.json_headers,
            )
            self.display_response(response, parse_json=False)

    def job_list(self, uuid=''):
        # The URL trailing slash is required without setting APPEND_SLASH=False
        url = f'''{self.conf['api_url_base']}/ce/job/'''
        if uuid:
            url = f'''{url}{uuid}/'''
        response = requests.get(
            url,
            headers=self.json_headers,
        )
        return self.display_response(response)

    def upload_list(self, uuid=''):
        # The URL trailing slash is required without setting APPEND_SLASH=False
        url = f'''{self.conf['api_url_base']}/ce/upload/'''
        if uuid:
            url = f'''{url}{uuid}/'''
        response = requests.get(
            url,
            headers=self.json_headers,
        )
        return self.display_response(response)

    def upload_file(self, file_path='', upload_path='', description='', public=False):
        url = f'''{self.conf['api_url_base']}/ce/upload/'''
        with open(file_path, 'rb') as fp:
            response = requests.put(
                url,
                headers={
                    'Authorization': f'''Token {self.api_token}''',
                },
                files={'file': fp},
                data={
                    'path': upload_path,
                    'description': description,
                    'public': public,
                },
            )
            return self.display_response(response)

    def upload_file_update(self, uuid, public=False):
        url = f'''{self.conf['api_url_base']}/ce/upload/{uuid}/'''
        response = requests.patch(
            url,
            headers={
                'Authorization': f'''Token {self.api_token}''',
            },
            data={
                'public': public,
            },
        )
        return self.display_response(response)

    def download_job_file(self, uuid, path, root_dir='/tmp/ce/jobs'):
        url = f'''{self.conf['url_base']}/ce/download/{uuid}/{path.strip('/')}'''
        file_path = os.path.join(root_dir, uuid, path.strip('/'))
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        with requests.get(url, headers=self.json_headers, stream=True) as request:
            request.raise_for_status()
            with open(file_path, 'wb') as fp:
                for chunk in request.iter_content(chunk_size=8192):
                    if chunk:
                        fp.write(chunk)

    def download_uploaded_file(self, uuid, root_dir='/tmp/ce/uploads'):
        url = f'''{self.conf['url_base']}/ce/download/{uuid}'''
        upload = self.upload_list(uuid=uuid)
        if 'path' not in upload:
            logger.error('Upload not found.')
            return
        path = upload['path'].strip('/')
        file_path = os.path.join(root_dir, uuid, path)
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        with requests.get(url, headers=self.json_headers, stream=True) as request:
            request.raise_for_status()
            with open(file_path, 'wb') as fp:
                for chunk in request.iter_content(chunk_size=8192):
                    if chunk:
                        fp.write(chunk)

    def delete_uploaded_file(self, uuid):
        url = f'''{self.conf['api_url_base']}/ce/upload/{uuid}/'''
        response = requests.delete(
            url,
            headers=self.json_headers,
        )
        return self.display_response(response, parse_json=False)

    def update_job(self, job_id, saved=False, public=False):
        url = f'''{self.conf['api_url_base']}/ce/job/{job_id}/'''
        response = requests.patch(
            url,
            headers=self.json_headers,
            json={
                'saved': saved,
                'public': public,
            },
        )
        return self.display_response(response)

    def list_users(self):
        url = f'''{self.conf['api_url_base']}/user/'''
        response = requests.get(
            url,
            headers=self.json_headers,
        )
        return self.display_response(response)

    def delete_user(self, username):
        url = f'''{self.conf['api_url_base']}/user/{username}/'''
        response = requests.delete(
            url,
            headers=self.json_headers,
        )
        return self.display_response(response)

    def create_user(self, username, password, is_staff=True):
        url = f'''{self.conf['api_url_base']}/user/'''
        response = requests.post(
            url,
            headers=self.json_headers,
            json={
                'username': username,
                'email': f'{username}@example.com',
                'is_staff': is_staff,
                'first_name': username,
                'last_name': username,
                'password': password,
            },
        )
        return self.display_response(response)


def purge_all_user_data(apis: Dict[str, CalculationEngineApi]) -> None:
    # Delete all jobs and uploaded files for the users
    for username, api in apis.items():
        logger.info(f'\nDelete all jobs "{username}":\n')
        api.job_delete_all()
        logger.info(f'\nList all jobs for "{username}":\n')
        api.job_list()
        logger.info(f'\nList current uploaded files for "{username}":\n')
        uploads = api.upload_list()
        logger.info(f'\nDelete all uploaded files for "{username}":\n')
        for upload in uploads['results']:
            api.delete_uploaded_file(uuid=upload['uuid'])


def get_admin_api():
    return CalculationEngineApi()


def get_staff_apis(username: str = '') -> dict:
    staff_api = {}
    if username:
        if username in Users.staff:
            api = CalculationEngineApi({
                'username': username,
                'password': Users.staff[username],
            })
            return {username: api}
        else:
            return {}
    for username, password in Users.staff.items():
        staff_api[username] = CalculationEngineApi({
            'username': username,
            'password': password,
        })
    return staff_api


def purge_and_create_users():
    logger.info('\nTest user delete/create endpoints:\n')
    admin_api = get_admin_api()
    for username, password in Users.staff.items():
        admin_api.delete_user(username)
        admin_api.create_user(username=username, password=password)


def run_and_monitor_workflow(
        api: CalculationEngineApi,
        config: str = '',
        description: str = '',
        loops: int = 5,
        sleep: int = 1,
        wait=True):
    import time
    username = api.conf['username']
    request_config = {
        "workflow": {
            "config": config
        }
    }
    job_response = api.job_create(description=description, config=request_config)
    if wait:
        job_id = job_response['uuid']
        count = 0
        while count < loops and job_response['status'] not in ['SUCCESS', 'FAILURE']:
            logger.info(f'\nStatus of current job for "{username}":\n')
            job_response = api.job_list(uuid=job_id)
            count += 1
            time.sleep(sleep)
    return job_response
