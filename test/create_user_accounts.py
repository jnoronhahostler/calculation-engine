from calculation_engine_api import get_admin_api
from calculation_engine_api import purge_and_create_users

if __name__ == "__main__":
    print('\nPurge and recreate user accounts:\n')
    admin_api = get_admin_api()
    purge_and_create_users()
    admin_api.list_users()
