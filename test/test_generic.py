import sys
import logging
import os
from pathlib import Path
import yaml
import subprocess
from calculation_engine_api import get_admin_api
from calculation_engine_api import purge_and_create_users
from time import sleep
from calculation_engine_api import get_staff_apis
from calculation_engine_api import run_and_monitor_workflow
logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
logger = logging.getLogger(__name__)


def download_script(job_id):
    script = f'''
        ORIG_DIR=$(pwd) && \
        cd /tmp && wget -r -nH -np --content-disposition \
        http://localhost:4000/files/jobs/{job_id}/ ;
        cd "${{ORIG_DIR}}"
    '''
    with open('/tmp/download_script.sh', 'w') as fp:
        fp.write(script)
    proc = subprocess.Popen(['bash', '/tmp/download_script.sh'])
    try:
        outs, errs = proc.communicate(timeout=60)
    except subprocess.TimeoutExpired:
        proc.kill()
        outs, errs = proc.communicate()
    print(outs, errs)


if __name__ == "__main__":
    print('\nPurge and recreate user accounts:\n')
    admin_api = get_admin_api()
    purge_and_create_users()
    admin_api.list_users()

    username = 'foo'
    api = get_staff_apis(username=username)[username]

    print(f'\nList current uploaded files for "{username}":\n')
    uploads = api.upload_list()
    print(f'\nDelete all uploaded files for "{username}":\n')
    for upload in uploads['results']:
        api.delete_uploaded_file(uuid=upload['uuid'])

    print('\nUpload the generic run.py script:\n')
    upload_response = api.upload_file(
        file_path='test/run.py',
        public=True,
        upload_path='/generic/run.py',
        description='Generic module runtime script',
    )
    upload_id = upload_response['uuid']
    # sys.exit()
    workflow_files = [
        'process_workflow.yaml',
    ]
    for workflow_file in workflow_files:
        with open(os.path.join(Path(__file__).resolve().parent, 'test_workflow_config', workflow_file)) as fp:
            wf_config = yaml.load(fp, Loader=yaml.SafeLoader)
        wf_config['processes'][0]['inputs']['script']['uuid'] = upload_id
        logger.debug(f'wf_config: {wf_config}')
        username = 'foo'
        api = get_staff_apis(username=username)[username]
        max_jobs = 1
        job_idx = 0
        while job_idx < max_jobs:
            print(f'Running job #{job_idx+1} of {max_jobs}...')
            job_response = run_and_monitor_workflow(
                api=api, config=wf_config,
                loops=10, sleep=10)
            job_id = job_response['uuid']
            # Save every other job
            if job_idx % 2 == 0:
                api.update_job(job_id=job_id, saved=True)
            api.job_delete(job_id)

            job_idx += 1
            sleep(5)
            # # Download and view all job output
            # download_script(job_id)
