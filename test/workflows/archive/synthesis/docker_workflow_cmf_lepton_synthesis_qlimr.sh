#!/bin/bash

# Exit on error
set -euo pipefail
SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"


# Create a directory for the workflow run
mkdir -p "${SCRIPT_PATH}"/workflow_cmf+lepton+synthesis+qlimr
cd "${SCRIPT_PATH}"/workflow_cmf+lepton+synthesis+qlimr

#-----------------------------------------------------------------------------#
# Define constants
#-----------------------------------------------------------------------------#

CMF_TEST_INPUT="${SCRIPT_PATH}/cmf/test/input"
CMF_TEST_OUTPUT="${SCRIPT_PATH}/cmf/test/output"
# Set default values for CMF Docker image, tag, and CMF YAML version
CMF_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/module-cmf/cmf"
CMF_DOCKER_IMAGE_TAG="v0.6.0"  # Use the specific compatible image tag
# Set default values for Lepton Docker image and tag
LEPTON_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/module-cmf/lepton-module"
LEPTON_DOCKER_IMAGE_TAG="v0.5.1"  # Use the specific compatible image tag
LEPTON_TEST_INPUT="${SCRIPT_PATH}/lepton-module/input"
LEPTON_TEST_OUTPUT="${SCRIPT_PATH}/lepton-module/output"
RUN_NAME_BARYONS_EOS="cmf_baryons"
RUN_NAME_QUARKS_EOS="cmf_quarks"
# Set default values for Synthesis Docker image and tag
SYNTHESIS_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/eos-synthesis"
SYNTHESIS_DOCKER_IMAGE_TAG="v0.1.5"  # Use the specific compatible image tag
SYNTHESIS_TEST_INPUT="${SCRIPT_PATH}/eos-synthesis/test/input"
SYNTHESIS_TEST_OUTPUT="${SCRIPT_PATH}/eos-synthesis/test/output"
# Set default values for QLIMR Docker image and tag
QLIMR_TEST_INPUT="${SCRIPT_PATH}/qlimr/test/input"
QLIMR_TEST_OUTPUT="${SCRIPT_PATH}/qlimr/test/output"
QLIMR_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/ns-qlimr/qlimr"
QLIMR_DOCKER_IMAGE_TAG="v0.2.1"  # Use the specific compatible image tag

#-----------------------------------------------------------------------------#
# Pull Docker images
#-----------------------------------------------------------------------------#

# Pull CMF Docker image
docker pull "${CMF_DOCKER_IMAGE_NAME}:${CMF_DOCKER_IMAGE_TAG}"
# Pull Lepton Docker image
docker pull "${LEPTON_DOCKER_IMAGE_NAME}:${LEPTON_DOCKER_IMAGE_TAG}"
# Pull Synthesis Docker image
docker pull "${SYNTHESIS_DOCKER_IMAGE_NAME}:${SYNTHESIS_DOCKER_IMAGE_TAG}"
# Pull QLIMR Docker image
docker pull "${QLIMR_DOCKER_IMAGE_NAME}:${QLIMR_DOCKER_IMAGE_TAG}"



#-----------------------------------------------------------------------------#
# CMF execution
#-----------------------------------------------------------------------------#

# Create test directories
mkdir -p "${CMF_TEST_INPUT}"
mkdir -p "${CMF_TEST_OUTPUT}"

# Create config file inside container, this could be done outside the container too
# Important: container is already in /home/cmf/src/ (there are some relative paths tied to this)
docker run --rm --name cmf \
  -v "${CMF_TEST_INPUT}:/home/cmf/input" \
  -v "${CMF_TEST_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG python3 create_config.py \
                                                                --muB_begin=940.0 \
                                                                --muB_end=1700.0 \
                                                                --muB_step=20.0 \
                                                                --muQ_begin=-350.0 \
                                                                --muQ_end=0.0 \
                                                                --muQ_step=25.0 \
                                                                --muS_begin=0.0 \
                                                                --muS_end=2.0 \
                                                                --muS_step=100.0 \
                                                                --use_octet=true \
                                                                --use_hyperons=true \
                                                                --use_decuplet=false \
                                                                --use_quarks=true \
# *todo* add all parameters here!

# Run the Docker container (preprocess, run CMF, clean output, postprocess)
docker run --rm --name cmf \
  -v "${CMF_TEST_INPUT}:/home/cmf/input" \
  -v "${CMF_TEST_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG python3 yaml_preprocess.py

docker run --rm --name cmf \
  -v "${CMF_TEST_INPUT}:/home/cmf/input" \
  -v "${CMF_TEST_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG cmf

docker run --rm --name cmf \
  -v "${CMF_TEST_INPUT}:/home/cmf/input" \
  -v "${CMF_TEST_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG python3 clean_output.py

docker run --rm --name cmf \
  -v "${CMF_TEST_INPUT}:/home/cmf/input" \
  -v "${CMF_TEST_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG python3 postprocess.py

#-----------------------------------------------------------------------------#
# lepton-module execution
#-----------------------------------------------------------------------------#

# Create test directories
mkdir -p "${LEPTON_TEST_INPUT}"
mkdir -p "${LEPTON_TEST_INPUT}/${RUN_NAME_BARYONS_EOS}"
mkdir -p "${LEPTON_TEST_INPUT}/${RUN_NAME_QUARKS_EOS}"
mkdir -p "${LEPTON_TEST_OUTPUT}"


# Copy the output of CMF into the input of lepton-module
## TO DO: adjust file names to match the output of CMF
cp ${CMF_TEST_OUTPUT}/CMF_output_for_Lepton_baryons.csv ${LEPTON_TEST_INPUT}/${RUN_NAME_BARYONS_EOS}/.

cp ${CMF_TEST_OUTPUT}/CMF_output_for_Lepton_quarks.csv ${LEPTON_TEST_INPUT}/${RUN_NAME_QUARKS_EOS}/.


# Run leptons for the hadron and quark EoS' in parallel. 
#First parallel job: Hadrons
(
    #Create the YAML configuration
    docker run --rm \
        -v "${LEPTON_TEST_INPUT}:/home/lepton/input" \
        -v "${LEPTON_TEST_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_generator.py \
                          --config_name="hadron_config.yaml" \
                          --run_name=${RUN_NAME_BARYONS_EOS} \
                          --use_beta_equilibrium=true \
                          --use_charge_neutrality=false \
                          --use_lepton_eos=false \
                          --eos_input_file="${RUN_NAME_BARYONS_EOS}/CMF_output_for_Lepton_baryons.csv" \
                          --output_densities=false \
                          --output_compOSE=false \
                          --use_electron=true \
                          --use_muon=true \
                          --use_tau=false \
                          --use_electron_neutrino=false \
                          --use_muon_neutrino=false \
                          --use_tau_neutrino=false \
                          --use_temperature=true \
                          --use_magnetic_field=false \
                          --debug=true

    docker run --rm \
      -v "${LEPTON_TEST_INPUT}:/home/lepton/input" \
      -v "${LEPTON_TEST_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_validator.py \
                                                            --input_config_name="hadron_config.yaml" \
                                                            --output_config_name="validated_hadron_config.yaml"
    # Run the lepton module
    docker run --rm \
      -v "${LEPTON_TEST_INPUT}:/home/lepton/input" \
      -v "${LEPTON_TEST_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG lepton validated_hadron

) &
# Second parallel job: Run quarks
(
  #Create the YAML configuration
    docker run --rm \
        -v "${LEPTON_TEST_INPUT}:/home/lepton/input" \
        -v "${LEPTON_TEST_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_generator.py \
                          --config_name="quark_config.yaml" \
                          --run_name=${RUN_NAME_QUARKS_EOS} \
                          --use_beta_equilibrium=true \
                          --use_charge_neutrality=false \
                          --use_lepton_eos=false \
                          --eos_input_file="${RUN_NAME_QUARKS_EOS}/CMF_output_for_Lepton_quarks.csv" \
                          --output_densities=false \
                          --output_compOSE=false \
                          --use_electron=true \
                          --use_muon=true \
                          --use_tau=false \
                          --use_electron_neutrino=false \
                          --use_muon_neutrino=false \
                          --use_tau_neutrino=false \
                          --use_temperature=true \
                          --use_magnetic_field=false \
                          --debug=true

    # Validate the YAML configuration
    docker run --rm \
      -v "${LEPTON_TEST_INPUT}:/home/lepton/input" \
      -v "${LEPTON_TEST_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_validator.py \
                                                            --input_config_name="quark_config.yaml" \
                                                            --output_config_name="validated_quark_config.yaml"

    # Run the lepton module
    docker run --rm \
      -v "${LEPTON_TEST_INPUT}:/home/lepton/input" \
      -v "${LEPTON_TEST_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG lepton validated_quark

) &

# Wait for both parallel jobs to finish
wait


mkdir -p "${SYNTHESIS_TEST_INPUT}"
mkdir -p "${SYNTHESIS_TEST_OUTPUT}"
mkdir -p "${SYNTHESIS_TEST_INPUT}/${RUN_NAME_BARYONS_EOS}"
mkdir -p "${SYNTHESIS_TEST_INPUT}/${RUN_NAME_QUARKS_EOS}"

cp ${LEPTON_TEST_OUTPUT}/${RUN_NAME_BARYONS_EOS}/* ${SYNTHESIS_TEST_INPUT}/${RUN_NAME_BARYONS_EOS}/.
cp ${LEPTON_TEST_OUTPUT}/${RUN_NAME_QUARKS_EOS}/* ${SYNTHESIS_TEST_INPUT}/${RUN_NAME_QUARKS_EOS}/.

# Create YAML configuration for the Synthesis module
docker run --rm \
        -v "${SYNTHESIS_TEST_INPUT}:/home/eos-synthesis/input" \
        -v "${SYNTHESIS_TEST_OUTPUT}:/home/eos-synthesis/output" \
        $SYNTHESIS_DOCKER_IMAGE_NAME:$SYNTHESIS_DOCKER_IMAGE_TAG python3 yaml_generator.py \
                            --synthesis_type=1 \
                            --debug=true \
                            --eos_input_files ${RUN_NAME_BARYONS_EOS}/beta_equilibrium.csv \
                                              ${RUN_NAME_QUARKS_EOS}/beta_equilibrium.csv \
                            --output_densities=false --output_flavor_equilibration=false --output_compOSE=false



# # Run the Docker container (validation, run eos-synthesis)
docker run --rm \
  -v "${SYNTHESIS_TEST_INPUT}:/home/eos-synthesis/input" \
  -v "${SYNTHESIS_TEST_OUTPUT}:/home/eos-synthesis/output" \
    $SYNTHESIS_DOCKER_IMAGE_NAME:$SYNTHESIS_DOCKER_IMAGE_TAG python3 yaml_validator.py

docker run --rm \
  -v "${SYNTHESIS_TEST_INPUT}:/home/eos-synthesis/input" \
  -v "${SYNTHESIS_TEST_OUTPUT}:/home/eos-synthesis/output" \
    $SYNTHESIS_DOCKER_IMAGE_NAME:$SYNTHESIS_DOCKER_IMAGE_TAG synthesis
# #-----------------------------------------------------------------------------#
# # QLIMR execution
# #-----------------------------------------------------------------------------#   

# Create test directories
mkdir -p "${QLIMR_TEST_INPUT}"
mkdir -p "${QLIMR_TEST_OUTPUT}"

cp ${SYNTHESIS_TEST_OUTPUT}/maxwell_eos.csv ${QLIMR_TEST_INPUT}/eos.csv

# Run the Docker container to attached Sly as the crust of the EoS provided
docker run --rm --name qlimr \
  -v "${QLIMR_TEST_INPUT}:/home/qlimr/input" \
  -v "${QLIMR_TEST_OUTPUT}:/home/qlimr/output" \
  $QLIMR_DOCKER_IMAGE_NAME:$QLIMR_DOCKER_IMAGE_TAG bash -c "python3 crust.py && 
                                                            python3 CSV_to_YAML_workflow.py && 
                                                            python3 create_config_lepton.py &&
                                                            cd ../api &&
                                                            python3 validation.py &&
                                                            cd ../src &&
                                                            python3 YAML_to_CSV.py &&
                                                            qlimr"  