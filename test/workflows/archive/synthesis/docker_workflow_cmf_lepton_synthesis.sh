#!/bin/bash

# Exit on error
set -euo pipefail
SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"

#-----------------------------------------------------------------------------#
# Define constants
#-----------------------------------------------------------------------------#

CMF_INPUT="${SCRIPT_PATH}/cmf/input"
CMF_OUTPUT="${SCRIPT_PATH}/cmf/output"
# Set default values for CMF Docker image, tag, and CMF YAML version
CMF_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/module-cmf/cmf"
CMF_DOCKER_IMAGE_TAG="v0.7.0"  # Use the specific compatible image tag
# Set default values for Lepton Docker image and tag
LEPTON_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/module-cmf/lepton-module"
LEPTON_DOCKER_IMAGE_TAG="v0.7.0"  # Use the specific compatible image tag
LEPTON_INPUT="${SCRIPT_PATH}/lepton-module/input"
LEPTON_OUTPUT="${SCRIPT_PATH}/lepton-module/output"
RUN_NAME_BARYONS_EOS="cmf_baryons"
RUN_NAME_QUARKS_EOS="cmf_quarks"
RUN_NAME_LEPTONS_EOS="leptons_eos"

# Set default values for Synthesis Docker image and tag
SYNTHESIS_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/eos-synthesis"
SYNTHESIS_DOCKER_IMAGE_TAG="v0.2.0"  # Use the specific compatible image tag
SYNTHESIS_INPUT="${SCRIPT_PATH}/eos-synthesis/input"
SYNTHESIS_OUTPUT="${SCRIPT_PATH}/eos-synthesis/output"

SYNTHESIS_TYPE=2
#-----------------------------------------------------------------------------#
# Pull Docker images
#-----------------------------------------------------------------------------#

# Pull CMF Docker image
docker pull "${CMF_DOCKER_IMAGE_NAME}:${CMF_DOCKER_IMAGE_TAG}"
# Pull Lepton Docker image
docker pull "${LEPTON_DOCKER_IMAGE_NAME}:${LEPTON_DOCKER_IMAGE_TAG}"
# Pull Synthesis Docker image
docker pull "${SYNTHESIS_DOCKER_IMAGE_NAME}:${SYNTHESIS_DOCKER_IMAGE_TAG}"


#-----------------------------------------------------------------------------#
# CMF execution
#-----------------------------------------------------------------------------#

# Create test directories
mkdir -p "${CMF_INPUT}"
mkdir -p "${CMF_OUTPUT}"

# Create config file inside container, this could be done outside the container too
# Important: container is already in /home/cmf/src/ (there are some relative paths tied to this)
docker run --rm --name cmf \
  -v "${CMF_INPUT}:/home/cmf/input" \
  -v "${CMF_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG python3 create_config.py \
                                                                --muB_begin=940.0 \
                                                                --muB_end=1800.0 \
                                                                --muB_step=15.0 \
                                                                --muQ_begin=-300.0 \
                                                                --muQ_end=0.0 \
                                                                --muQ_step=15.0 \
                                                                --muS_begin=0.0 \
                                                                --muS_end=2.0 \
                                                                --muS_step=100.0 \
                                                                --use_quarks=true \
                                                                --use_octet=true \
                                                                --use_decuplet=false \
                                                                --use_hyperons=true \
                                                                --vector_potential=4 \
                                                                --output_debug=true
# *todo* add all parameters here!

# Run the Docker container (preprocess, run CMF, clean output, postprocess)
docker run --rm --name cmf \
  -v "${CMF_INPUT}:/home/cmf/input" \
  -v "${CMF_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG python3 yaml_preprocess.py

docker run --rm --name cmf \
  -v "${CMF_INPUT}:/home/cmf/input" \
  -v "${CMF_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG cmf

docker run --rm --name cmf \
  -v "${CMF_INPUT}:/home/cmf/input" \
  -v "${CMF_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG python3 clean_output.py

docker run --rm --name cmf \
  -v "${CMF_INPUT}:/home/cmf/input" \
  -v "${CMF_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG python3 postprocess.py



#-----------------------------------------------------------------------------#
# lepton-module execution
#-----------------------------------------------------------------------------#

# Create test directories
mkdir -p "${LEPTON_INPUT}"
mkdir -p "${LEPTON_INPUT}/${RUN_NAME_BARYONS_EOS}"
mkdir -p "${LEPTON_INPUT}/${RUN_NAME_QUARKS_EOS}"
mkdir -p "${LEPTON_OUTPUT}"


# Copy the output of CMF into the input of lepton-module
## TO DO: adjust file names to match the output of CMF
cp ${CMF_OUTPUT}/CMF_output_for_Lepton_baryons.csv ${LEPTON_INPUT}/${RUN_NAME_BARYONS_EOS}/.

cp ${CMF_OUTPUT}/CMF_output_for_Lepton_quarks.csv ${LEPTON_INPUT}/${RUN_NAME_QUARKS_EOS}/.


# Run leptons for the hadron and quark EoS' in parallel. 
#First parallel job: Hadrons
(
    #Create the YAML configuration
    docker run --rm \
        -v "${LEPTON_INPUT}:/home/lepton/input" \
        -v "${LEPTON_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_generator.py \
                          --config_path="../input/${RUN_NAME_BARYONS_EOS}/" \
                          --run_name=${RUN_NAME_BARYONS_EOS} \
                          --use_beta_equilibrium=true \
                          --use_charge_neutrality=false \
                          --use_lepton_eos=false \
                          --eos_input_file="${RUN_NAME_BARYONS_EOS}/CMF_output_for_Lepton_baryons.csv" \
                          --output_particle_properties=false \
                          --output_compOSE=false \
                          --use_electron=true \
                          --use_muon=true \
                          --use_tau=false \
                          --use_electron_neutrino=false \
                          --use_muon_neutrino=false \
                          --use_tau_neutrino=false \
                          --use_temperature=true \
                          --use_magnetic_field=false \
                          --verbose=1

    docker run --rm \
      -v "${LEPTON_INPUT}:/home/lepton/input" \
      -v "${LEPTON_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_validator.py \
                                                            --input_config_path="../input/${RUN_NAME_BARYONS_EOS}/"

    # Run the lepton module
    docker run --rm \
      -v "${LEPTON_INPUT}:/home/lepton/input" \
      -v "${LEPTON_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG lepton "${RUN_NAME_BARYONS_EOS}"

) &
# Second parallel job: Run quarks
(
  #Create the YAML configuration
    docker run --rm \
        -v "${LEPTON_INPUT}:/home/lepton/input" \
        -v "${LEPTON_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_generator.py \
                          --config_path="../input/${RUN_NAME_QUARKS_EOS}/" \
                          --run_name=${RUN_NAME_QUARKS_EOS} \
                          --use_beta_equilibrium=true \
                          --use_charge_neutrality=false \
                          --use_lepton_eos=false \
                          --eos_input_file="${RUN_NAME_QUARKS_EOS}/CMF_output_for_Lepton_quarks.csv" \
                          --output_particle_properties=false \
                          --output_compOSE=false \
                          --use_electron=true \
                          --use_muon=true \
                          --use_tau=false \
                          --use_electron_neutrino=false \
                          --use_muon_neutrino=false \
                          --use_tau_neutrino=false \
                          --use_temperature=true \
                          --use_magnetic_field=false \
                          --verbose=1

    # Validate the YAML configuration
    docker run --rm \
      -v "${LEPTON_INPUT}:/home/lepton/input" \
      -v "${LEPTON_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_validator.py \
                                                            --input_config_path="../input/${RUN_NAME_QUARKS_EOS}/" 
                                                            

    # Run the lepton module
    docker run --rm \
      -v "${LEPTON_INPUT}:/home/lepton/input" \
      -v "${LEPTON_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG lepton "${RUN_NAME_QUARKS_EOS}" 

) &

if [ "$SYNTHESIS_TYPE" -eq 2 ]; then
(
    mkdir -p "${LEPTON_INPUT}/${RUN_NAME_LEPTONS_EOS}"

    #Create the YAML configuration
    docker run --rm \
        -v "${LEPTON_INPUT}:/home/lepton/input" \
        -v "${LEPTON_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_generator.py \
                          --config_path="../input/${RUN_NAME_LEPTONS_EOS}/" \
                          --run_name=${RUN_NAME_LEPTONS_EOS} \
                          --use_beta_equilibrium=false \
                          --use_charge_neutrality=false \
                          --use_lepton_eos=true \
                          --output_particle_properties=false \
                          --output_compOSE=false \
                          --use_electron=true \
                          --use_muon=true \
                          --use_tau=false \
                          --use_electron_neutrino=false \
                          --use_muon_neutrino=false \
                          --use_tau_neutrino=false \
                          --use_temperature=true \
                          --use_magnetic_field=false \
                          --verbose=1 \
                          --temperature_initial=0.0 \
                          --temperature_final=1.0 \
                          --temperature_step=10.0 \
                          --electron_cp_initial=0.0 \
                          --electron_cp_final=500.0 \
                          --electron_cp_step=1.0 \
                          --electron_neutrino_cp_initial=0.0 \
                          --electron_neutrino_cp_final=1.0 \
                          --electron_neutrino_cp_step=20.0

    docker run --rm \
      -v "${LEPTON_INPUT}:/home/lepton/input" \
      -v "${LEPTON_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_validator.py \
                                                            --input_config_path="../input/${RUN_NAME_LEPTONS_EOS}/"
    # Run the lepton module
    docker run --rm \
      -v "${LEPTON_INPUT}:/home/lepton/input" \
      -v "${LEPTON_OUTPUT}:/home/lepton/output" \
        $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG lepton "${RUN_NAME_LEPTONS_EOS}"


)&

fi



# Wait for both parallel jobs to finish
wait


mkdir -p "${SYNTHESIS_INPUT}"
mkdir -p "${SYNTHESIS_OUTPUT}"
cp -r ${LEPTON_OUTPUT}/${RUN_NAME_BARYONS_EOS} ${SYNTHESIS_INPUT}/${RUN_NAME_BARYONS_EOS}
cp -r ${LEPTON_OUTPUT}/${RUN_NAME_QUARKS_EOS} ${SYNTHESIS_INPUT}/${RUN_NAME_QUARKS_EOS}

SYNTHESIS_FILES="${RUN_NAME_BARYONS_EOS}/beta_equilibrium.csv \
                ${RUN_NAME_QUARKS_EOS}/beta_equilibrium.csv"

if [ "$SYNTHESIS_TYPE" -eq 2 ]; then
    cp -r ${LEPTON_OUTPUT}/${RUN_NAME_LEPTONS_EOS} ${SYNTHESIS_INPUT}/${RUN_NAME_LEPTONS_EOS}
    cp ${CMF_OUTPUT}/CMF_output_for_Lepton_baryons.csv ${SYNTHESIS_INPUT}/${RUN_NAME_BARYONS_EOS}/.
    cp ${CMF_OUTPUT}/CMF_output_for_Lepton_quarks.csv ${SYNTHESIS_INPUT}/${RUN_NAME_QUARKS_EOS}/.
    SYNTHESIS_FILES="${RUN_NAME_BARYONS_EOS}/beta_equilibrium.csv \
                     ${RUN_NAME_QUARKS_EOS}/beta_equilibrium.csv \
                     ${RUN_NAME_LEPTONS_EOS}/lepton_eos.csv \
                     ${RUN_NAME_BARYONS_EOS}/CMF_output_for_Lepton_baryons.csv \
                     ${RUN_NAME_QUARKS_EOS}/CMF_output_for_Lepton_quarks.csv"
fi

# Create YAML configuration for the Synthesis module
docker run --rm \
        -v "${SYNTHESIS_INPUT}:/home/eos-synthesis/input" \
        -v "${SYNTHESIS_OUTPUT}:/home/eos-synthesis/output" \
        $SYNTHESIS_DOCKER_IMAGE_NAME:$SYNTHESIS_DOCKER_IMAGE_TAG python3 yaml_generator.py \
                            --synthesis_type=$SYNTHESIS_TYPE \
                            --verbose=3 \
                            --eos_input_files $SYNTHESIS_FILES \
                            --output_particle_properties=false --output_flavor_equilibration=false --output_compOSE=false



# # Run the Docker container (validation, run eos-synthesis)
docker run --rm \
  -v "${SYNTHESIS_INPUT}:/home/eos-synthesis/input" \
  -v "${SYNTHESIS_OUTPUT}:/home/eos-synthesis/output" \
    $SYNTHESIS_DOCKER_IMAGE_NAME:$SYNTHESIS_DOCKER_IMAGE_TAG python3 yaml_validator.py

docker run --rm \
  -v "${SYNTHESIS_INPUT}:/home/eos-synthesis/input" \
  -v "${SYNTHESIS_OUTPUT}:/home/eos-synthesis/output" \
    $SYNTHESIS_DOCKER_IMAGE_NAME:$SYNTHESIS_DOCKER_IMAGE_TAG synthesis