#!/bin/bash

# Exit on error
set -euo pipefail

#-----------------------------------------------------------------------------#
# Define constants
#-----------------------------------------------------------------------------#

SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"

UTK_OUTPUT="${SCRIPT_PATH}/utk/"
UTK_DOCKER_IMAGE_NAME="nostrad1/utk-eos"
UTK_DOCKER_IMAGE_TAG="v2"  # Use the specific compatible image tag
# Set default values for Lepton Docker image and tag
LEPTON_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/module-cmf/lepton-module"
LEPTON_DOCKER_IMAGE_TAG="v0.7.0"  # Use the specific compatible image tag
LEPTON_INPUT="${SCRIPT_PATH}/lepton-module/input"
LEPTON_OUTPUT="${SCRIPT_PATH}/lepton-module/output"

#-----------------------------------------------------------------------------#
# Pull Docker images
#-----------------------------------------------------------------------------#

# Pull CMF Docker image
docker pull "${UTK_DOCKER_IMAGE_NAME}:${UTK_DOCKER_IMAGE_TAG}"
# Pull Lepton Docker image
docker pull "${LEPTON_DOCKER_IMAGE_NAME}:${LEPTON_DOCKER_IMAGE_TAG}"

#-----------------------------------------------------------------------------#
# UTK execution
#-----------------------------------------------------------------------------#

# Create test directories
mkdir -p "${UTK_OUTPUT}"


docker run -it --rm \
  -v "${UTK_OUTPUT}:/opt/eos/output" \
  $UTK_DOCKER_IMAGE_NAME:$UTK_DOCKER_IMAGE_TAG sh -c "mkdir -p /opt/eos/output && make enn_fid_lep && cp utk_eos.csv output/."

#-----------------------------------------------------------------------------#
# lepton-module execution
#-----------------------------------------------------------------------------#

# Create test directories
mkdir -p "${LEPTON_INPUT}"
mkdir -p "${LEPTON_OUTPUT}"

# Copy the output of UTK into the input of lepton-module
cp ${UTK_OUTPUT}/utk_eos.csv ${LEPTON_INPUT}/UTK_data.csv



# Create config file inside container, this could be done outside the container too
docker run -it --rm \
    -v "${LEPTON_INPUT}:/home/lepton/input" \
    -v "${LEPTON_OUTPUT}:/home/lepton/output" \
    $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_generator.py \
                          --eos_input_file="UTK_data.csv" \
                          --use_beta_equilibrium=true \
                          --use_charge_neutrality=false \
                          --use_fixed_lepton_fraction=false \
                          --lepton_fraction=0.5 \
                          --use_electron=true \
                          --use_muon=true \
                          --use_tau=false \
                          --use_electron_neutrino=false \
                          --use_muon_neutrino=false \
                          --use_tau_neutrino=false \
                          --use_lepton_eos=false \
                          --use_temperature=true \
                          --use_magnetic_field=false \
                          --B_value=0 \
                          --use_anomalous_magnetic_moment=false \
                          --verbose=2

# Run the Docker container (validation, run lepton)
docker run -it --rm \
  -v "${LEPTON_INPUT}:/home/lepton/input" \
  -v "${LEPTON_OUTPUT}:/home/lepton/output" \
    $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_validator.py

docker run -it --rm \
  -v "${LEPTON_INPUT}:/home/lepton/input" \
  -v "${LEPTON_OUTPUT}:/home/lepton/output" \
    $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG lepton
