#!/bin/bash

# Exit on error
set -euo pipefail

# Get the path of the script
SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"

# Change directory to the script path
cd "${SCRIPT_PATH}/workflow_cmf+lepton+qlimr"

# Create a directory for the workflow run
mkdir -p workflow_cmf+lepton+qlimr
cd workflow_cmf+lepton+qlimr

#-----------------------------------------------------------------------------#
# Define constants
#-----------------------------------------------------------------------------#

CMF_TEST_INPUT="${SCRIPT_PATH}/workflow_cmf+lepton+qlimr/cmf/test/input"
CMF_TEST_OUTPUT="${SCRIPT_PATH}/workflow_cmf+lepton+qlimr/cmf/test/output"
# Set default values for CMF Docker image, tag, and CMF YAML version
CMF_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/module-cmf/cmf"
CMF_DOCKER_IMAGE_TAG="v0.5.0"  # Use the specific compatible image tag
CMF_YAML_VERSION=2
# Set default values for Lepton Docker image and tag
LEPTON_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/module-cmf/lepton-module"
LEPTON_DOCKER_IMAGE_TAG="v0.4.4"  # Use the specific compatible image tag
LEPTON_TEST_INPUT="${SCRIPT_PATH}/workflow_cmf+lepton+qlimr/lepton-module/test/input"
LEPTON_TEST_OUTPUT="${SCRIPT_PATH}/workflow_cmf+lepton+qlimr/lepton-module/test/output"
# Set default values for QLIMR Docker image and tag
QLIMR_TEST_INPUT="${SCRIPT_PATH}/workflow_cmf+lepton+qlimr/qlimr/test/input"
QLIMR_TEST_OUTPUT="${SCRIPT_PATH}/workflow_cmf+lepton+qlimr/qlimr/test/output"
QLIMR_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/ns-qlimr/qlimr"
QLIMR_DOCKER_IMAGE_TAG="v0.2.0"  # Use the specific compatible image tag

#-----------------------------------------------------------------------------#
# Pull Docker images
#-----------------------------------------------------------------------------#

# Pull CMF Docker image
docker pull "${CMF_DOCKER_IMAGE_NAME}:${CMF_DOCKER_IMAGE_TAG}"
# Pull Lepton Docker image
docker pull "${LEPTON_DOCKER_IMAGE_NAME}:${LEPTON_DOCKER_IMAGE_TAG}"
# Pull QLIMR Docker image
docker pull "${QLIMR_DOCKER_IMAGE_NAME}:${QLIMR_DOCKER_IMAGE_TAG}"

#-----------------------------------------------------------------------------#
# CMF execution
#-----------------------------------------------------------------------------#

# Create test directories
mkdir -p "${CMF_TEST_INPUT}"
mkdir -p "${CMF_TEST_OUTPUT}"

# Create config file inside container, this could be done outside the container too
# Important: container is already in /home/cmf/src/ (there are some relative paths tied to this)
docker run -it --rm --name cmf \
  -v "${CMF_TEST_INPUT}:/home/cmf/input" \
  -v "${CMF_TEST_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG python3 create_config_v2.py \
                                                                --muB_begin=940.0 \
                                                                --muB_end=1800.0 \
                                                                --muB_step=15.0 \
                                                                --muQ_begin=-250.0 \
                                                                --muQ_end=0.0 \
                                                                --muQ_step=25.0 \
                                                                --muS_begin=0.0 \
                                                                --muS_end=2.0 \
                                                                --muS_step=100.0 \
                                                                --use_decuplet=false \
                                                                --use_quarks=false
# *todo* add all parameters here!

# Run the Docker container (preprocess, run CMF, clean output, postprocess)
docker run --rm --name cmf \
  -v "${CMF_TEST_INPUT}:/home/cmf/input" \
  -v "${CMF_TEST_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG bash -c "python3 yaml_preprocess.py --version $CMF_YAML_VERSION && 
                                                        cmf &&
                                                        python3 clean_output.py &&
                                                        python3 yaml_postprocess.py"

#-----------------------------------------------------------------------------#
# lepton-module execution
#-----------------------------------------------------------------------------#

# Create test directories
mkdir -p "${LEPTON_TEST_INPUT}"
mkdir -p "${LEPTON_TEST_OUTPUT}"

# Copy the output of CMF into the input of lepton-module
cp ${CMF_TEST_OUTPUT}/CMF_output_for_Lepton_formatted.dat ${LEPTON_TEST_INPUT}/CMF_data.dat

# Create config file inside container, this could be done outside the container too
docker run -it --rm \
    -v "${LEPTON_TEST_INPUT}:/home/lepton/input" \
    -v "${LEPTON_TEST_OUTPUT}:/home/lepton/output" \
    $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_generator.py \
                          --eos_input_file="CMF_data.dat" \
                          --use_beta_equilibrium=true \
                          --use_charge_neutrality=true \
                          --use_fixed_lepton_fraction=false \
                          --lepton_fraction=0.5 \
                          --use_electron=true \
                          --use_muon=true \
                          --use_tau=false \
                          --use_electron_neutrino=false \
                          --use_muon_neutrino=false \
                          --use_tau_neutrino=false \
                          --use_lepton_eos=false \
                          --use_temperature=true \
                          --use_magnetic_field=false \
                          --B_value=0 \
                          --use_anomalous_magnetic_moment=false \
                          --debug=true

# Run the Docker container (validation, run lepton)
docker run --rm --name lepton \
  -v "${LEPTON_TEST_INPUT}:/home/lepton/input" \
  -v "${LEPTON_TEST_OUTPUT}:/home/lepton/output" \
  $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG bash -c "python3 yaml_validation.py && 
                                                              lepton"

#-----------------------------------------------------------------------------#
# QLIMR execution
#-----------------------------------------------------------------------------#   

# Create test directories
mkdir -p "${QLIMR_TEST_INPUT}"
mkdir -p "${QLIMR_TEST_OUTPUT}"

cp ${LEPTON_TEST_OUTPUT}/beta_equilibrium.dat ${QLIMR_TEST_INPUT}/beta_equilibrium.dat

# Run the Docker container to attached Sly as the crust of the EoS provided
docker run --rm --name qlimr \
  -v "${QLIMR_TEST_INPUT}:/home/qlimr/input" \
  -v "${QLIMR_TEST_OUTPUT}:/home/qlimr/output" \
  $QLIMR_DOCKER_IMAGE_NAME:$QLIMR_DOCKER_IMAGE_TAG bash -c "python3 crust.py && 
                                                            python3 CSV_to_YAML_workflow.py && 
                                                            python3 create_config_lepton.py &&
                                                            cd ../api &&
                                                            python3 validation.py &&
                                                            cd ../src &&
                                                            python3 YAML_to_CSV.py &&
                                                            qlimr"  