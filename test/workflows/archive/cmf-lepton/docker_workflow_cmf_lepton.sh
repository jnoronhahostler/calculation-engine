#!/bin/bash
# Exit on error
set -euo pipefail
SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"

# Create a directory for the workflow run
mkdir -p "${SCRIPT_PATH}"/workflow_cmf+lepton
cd "${SCRIPT_PATH}"/workflow_cmf+lepton

#-----------------------------------------------------------------------------#
# Define constants
#-----------------------------------------------------------------------------#

CMF_TEST_INPUT="${SCRIPT_PATH}/cmf/test/input"
CMF_TEST_OUTPUT="${SCRIPT_PATH}/cmf/test/output"
# Set default values for CMF Docker image, tag, and CMF YAML version
CMF_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/module-cmf/cmf"
CMF_DOCKER_IMAGE_TAG="v0.6.0"  # Use the specific compatible image tag
# Set default values for Lepton Docker image and tag
LEPTON_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/module-cmf/lepton-module"
LEPTON_DOCKER_IMAGE_TAG="v0.5.1"  # Use the specific compatible image tag
LEPTON_TEST_INPUT="${SCRIPT_PATH}/lepton-module/test/input"
LEPTON_TEST_OUTPUT="${SCRIPT_PATH}/lepton-module/test/output"

#-----------------------------------------------------------------------------#
# Pull Docker images
#-----------------------------------------------------------------------------#

# Pull CMF Docker image
docker pull "${CMF_DOCKER_IMAGE_NAME}:${CMF_DOCKER_IMAGE_TAG}"

# Pull Lepton Docker image
docker pull "${LEPTON_DOCKER_IMAGE_NAME}:${LEPTON_DOCKER_IMAGE_TAG}"

#-----------------------------------------------------------------------------#
# CMF execution
#-----------------------------------------------------------------------------#

# Create test directories
mkdir -p "${CMF_TEST_INPUT}"
mkdir -p "${CMF_TEST_OUTPUT}"

# Create config file inside container, this could be done outside the container too
# Important: container is already in /home/cmf/src/ (there are some relative paths tied to this)
docker run -it --rm --name cmf \
  -v "${CMF_TEST_INPUT}:/home/cmf/input" \
  -v "${CMF_TEST_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG python3 create_config.py \
                                                                --muB_begin=940.0 \
                                                                --muB_end=1700.0 \
                                                                --muB_step=10.0 \
                                                                --muQ_begin=-300.0 \
                                                                --muQ_end=0.0 \
                                                                --muQ_step=20.0 \
                                                                --muS_begin=0.0 \
                                                                --muS_end=2.0 \
                                                                --muS_step=100.0 \
                                                                --use_octet=true \
                                                                --use_hyperons=true \
                                                                --use_decuplet=false \
                                                                --use_quarks=true \
# *todo* add all parameters here!

# Run the Docker container (preprocess, run CMF, clean output, postprocess)
docker run -it --rm --name cmf \
  -v "${CMF_TEST_INPUT}:/home/cmf/input" \
  -v "${CMF_TEST_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG python3 yaml_preprocess.py

docker run -it --rm --name cmf \
  -v "${CMF_TEST_INPUT}:/home/cmf/input" \
  -v "${CMF_TEST_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG cmf

docker run -it --rm --name cmf \
  -v "${CMF_TEST_INPUT}:/home/cmf/input" \
  -v "${CMF_TEST_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG python3 clean_output.py

docker run -it --rm --name cmf \
  -v "${CMF_TEST_INPUT}:/home/cmf/input" \
  -v "${CMF_TEST_OUTPUT}:/home/cmf/output" \
  $CMF_DOCKER_IMAGE_NAME:$CMF_DOCKER_IMAGE_TAG python3 postprocess.py

#-----------------------------------------------------------------------------#
# lepton-module execution
#-----------------------------------------------------------------------------#

# Create test directories
mkdir -p "${LEPTON_TEST_INPUT}"
mkdir -p "${LEPTON_TEST_OUTPUT}"

# Copy the output of CMF into the input of lepton-module
cp ${CMF_TEST_OUTPUT}/CMF_output_for_Lepton_baryons.csv ${LEPTON_TEST_INPUT}/.

# Create config file inside container, this could be done outside the container too
docker run -it --rm \
    -v "${LEPTON_TEST_INPUT}:/home/lepton/input" \
    -v "${LEPTON_TEST_OUTPUT}:/home/lepton/output" \
    $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_generator.py \
                          --eos_input_file="CMF_output_for_Lepton_baryons.csv" \
                          --use_beta_equilibrium=true \
                          --use_charge_neutrality=true \
                          --use_fixed_lepton_fraction=false \
                          --lepton_fraction=0.5 \
                          --use_electron=true \
                          --use_muon=true \
                          --use_tau=false \
                          --use_electron_neutrino=false \
                          --use_muon_neutrino=false \
                          --use_tau_neutrino=false \
                          --use_lepton_eos=false \
                          --use_temperature=true \
                          --use_magnetic_field=false \
                          --B_value=0 \
                          --use_anomalous_magnetic_moment=false \
                          --debug=true

# Run the Docker container (validation, run lepton)
docker run -it --rm \
  -v "${LEPTON_TEST_INPUT}:/home/lepton/input" \
  -v "${LEPTON_TEST_OUTPUT}:/home/lepton/output" \
    $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_validator.py

docker run -it --rm \
  -v "${LEPTON_TEST_INPUT}:/home/lepton/input" \
  -v "${LEPTON_TEST_OUTPUT}:/home/lepton/output" \
    $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG lepton
