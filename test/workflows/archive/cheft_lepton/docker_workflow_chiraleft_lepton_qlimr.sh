#!/bin/bash

# Exit on error
set -euo pipefail

#-----------------------------------------------------------------------------#
# Define constants
#-----------------------------------------------------------------------------#

SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"

CHIRAL_EFT_INPUT="${SCRIPT_PATH}/cheft/input"
CHIRAL_EFT_OUTPUT="${SCRIPT_PATH}/cheft/output"
CHIRAL_EFT_DATA="${SCRIPT_PATH}/cheft/data"
CHIRAL_EFT_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/chiral-eft-eos/chiral_eft_eos"
CHIRAL_EFT_DOCKER_IMAGE_TAG="v0.1.0"  # Use the specific compatible image tag
# Set default values for Lepton Docker image and tag
LEPTON_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/module-cmf/lepton-module"
LEPTON_DOCKER_IMAGE_TAG="v0.6.1"  # Use the specific compatible image tag
LEPTON_INPUT="${SCRIPT_PATH}/lepton-module/input"
LEPTON_OUTPUT="${SCRIPT_PATH}/lepton-module/output"
# Set default values for QLIMR Docker image and tag
QLIMR_INPUT="${SCRIPT_PATH}/qlimr/test/input"
QLIMR_OUTPUT="${SCRIPT_PATH}/qlimr/test/output"
QLIMR_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/ns-qlimr/qlimr"
QLIMR_DOCKER_IMAGE_TAG="v0.2.1"  # Use the specific compatible image tag


#-----------------------------------------------------------------------------#
# Pull Docker images
#-----------------------------------------------------------------------------#

# Pull CMF Docker image
docker pull "${CHIRAL_EFT_DOCKER_IMAGE_NAME}:${CHIRAL_EFT_DOCKER_IMAGE_TAG}"
# Pull Lepton Docker image
docker pull "${LEPTON_DOCKER_IMAGE_NAME}:${LEPTON_DOCKER_IMAGE_TAG}"


#-----------------------------------------------------------------------------#
# CHIRAL_EFT execution
#-----------------------------------------------------------------------------#

# Create test directories
mkdir -p "${CHIRAL_EFT_DATA}"

#download the input data
wget -nc https://cloud.musesframework.io/s/fcLyoEFKRiZiQsX/download/N3LO_450.h5 -P "${CHIRAL_EFT_DATA}"

sudo chown -R 1000:1000 cheft/

# Run the Docker container
docker run -it --rm \
    -u $(id -u):$(id -g)\
    -v "${CHIRAL_EFT_DATA}:/home/chiraleft/data" \
    $CHIRAL_EFT_DOCKER_IMAGE_NAME:$CHIRAL_EFT_DOCKER_IMAGE_TAG python3 create_config.py --iso_start=0.0 --iso_end=1.0


docker run -it --rm \
    -v "${CHIRAL_EFT_DATA}:/home/chiraleft/data" \
    -v "${CHIRAL_EFT_INPUT}:/home/chiraleft/input" \
    -v "${CHIRAL_EFT_OUTPUT}:/home/chiraleft/output" \
    $CHIRAL_EFT_DOCKER_IMAGE_NAME:$CHIRAL_EFT_DOCKER_IMAGE_TAG make run


docker run -it --rm \
    -v "${CHIRAL_EFT_INPUT}:/home/chiraleft/input" \
    -v "${CHIRAL_EFT_OUTPUT}:/home/chiraleft/output" \
    $CHIRAL_EFT_DOCKER_IMAGE_NAME:$CHIRAL_EFT_DOCKER_IMAGE_TAG python3 clean_output.py


# #-----------------------------------------------------------------------------#
# # lepton-module execution
# #-----------------------------------------------------------------------------#

# Create test directories
mkdir -p "${LEPTON_INPUT}"
mkdir -p "${LEPTON_OUTPUT}"

# # Copy the output of CHIRAL_EFT into the input of lepton-module
cp ${CHIRAL_EFT_OUTPUT}/output_lepton.csv ${LEPTON_INPUT}/chiraleft_grid.csv



# Create config file inside container, this could be done outside the container too
docker run -it --rm \
    -v "${LEPTON_INPUT}:/home/lepton/input" \
    -v "${LEPTON_OUTPUT}:/home/lepton/output" \
    $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_generator.py \
                          --eos_input_file="chiraleft_grid.csv" \
                          --use_beta_equilibrium=true \
                          --use_charge_neutrality=false \
                          --use_fixed_lepton_fraction=false \
                          --use_electron=true \
                          --use_muon=true \
                          --use_tau=false \
                          --use_electron_neutrino=false \
                          --use_muon_neutrino=false \
                          --use_tau_neutrino=false \
                          --use_lepton_eos=false \
                          --use_temperature=true \
                          --use_magnetic_field=false \
                          --B_value=0 \
                          --use_anomalous_magnetic_moment=false \
                          --verbose=4

# Run the Docker container (validation, run lepton)
docker run -it --rm \
  -v "${LEPTON_INPUT}:/home/lepton/input" \
  -v "${LEPTON_OUTPUT}:/home/lepton/output" \
    $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_validator.py

docker run -it --rm \
  -v "${LEPTON_INPUT}:/home/lepton/input" \
  -v "${LEPTON_OUTPUT}:/home/lepton/output" \
    $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG lepton


# #-----------------------------------------------------------------------------#
# # QLIMR execution
# #-----------------------------------------------------------------------------#   

# Create test directories
mkdir -p "${QLIMR_INPUT}"
mkdir -p "${QLIMR_OUTPUT}"

cp ${LEPTON_OUTPUT}/beta_equilibrium.csv ${QLIMR_INPUT}/eos.csv

# Run the Docker container to attached Sly as the crust of the EoS provided
docker run --rm --name qlimr \
  -v "${QLIMR_INPUT}:/home/qlimr/input" \
  -v "${QLIMR_OUTPUT}:/home/qlimr/output" \
  $QLIMR_DOCKER_IMAGE_NAME:$QLIMR_DOCKER_IMAGE_TAG bash -c "python3 crust.py && 
                                                            python3 CSV_to_YAML_workflow.py && 
                                                            python3 create_config_lepton.py &&
                                                            cd ../api &&
                                                            python3 validation.py &&
                                                            cd ../src &&
                                                            python3 YAML_to_CSV.py &&
                                                            qlimr"  
