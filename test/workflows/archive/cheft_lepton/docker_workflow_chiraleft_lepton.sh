#!/bin/bash

# Exit on error
set -euo pipefail

#-----------------------------------------------------------------------------#
# Define constants
#-----------------------------------------------------------------------------#

SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"

CHIRAL_EFT_INPUT="${SCRIPT_PATH}/cheft/input"
CHIRAL_EFT_OUTPUT="${SCRIPT_PATH}/cheft/output"
CHIRAL_EFT_DATA="${SCRIPT_PATH}/cheft/data"
CHIRAL_EFT_LOCAL="${SCRIPT_PATH}/cheft"

CHIRAL_EFT_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/chiral-eft-eos/chiral_eft_eos"
CHIRAL_EFT_DOCKER_IMAGE_TAG="v0.2.1"  # Use the specific compatible image tag
# Set default values for Lepton Docker image and tag
LEPTON_DOCKER_IMAGE_NAME="registry.gitlab.com/nsf-muses/module-cmf/lepton-module"
LEPTON_DOCKER_IMAGE_TAG="v0.7.0"  # Use the specific compatible image tag
LEPTON_LOCAL="${SCRIPT_PATH}/lepton-module"
LEPTON_INPUT="${SCRIPT_PATH}/lepton-module/input"
LEPTON_OUTPUT="${SCRIPT_PATH}/lepton-module/output"

#-----------------------------------------------------------------------------#
# Pull Docker images
#-----------------------------------------------------------------------------#

# Pull CEFT Docker image
docker pull "${CHIRAL_EFT_DOCKER_IMAGE_NAME}:${CHIRAL_EFT_DOCKER_IMAGE_TAG}"
# Pull Lepton Docker image
docker pull "${LEPTON_DOCKER_IMAGE_NAME}:${LEPTON_DOCKER_IMAGE_TAG}"


#-----------------------------------------------------------------------------#
# CHIRAL_EFT execution
#-----------------------------------------------------------------------------#

# Create test directories
mkdir -p "${CHIRAL_EFT_DATA}"

#download the input data
wget -nc https://cloud.musesframework.io/s/fcLyoEFKRiZiQsX/download/N3LO_450.h5 -P "${CHIRAL_EFT_DATA}"

chmod -R a+rwX "${CHIRAL_EFT_LOCAL}"
chmod -R a+rwX "${LEPTON_LOCAL}"

# Run the Docker container
docker run -it --rm \
    -u $(id -u):$(id -g)\
    -v "${CHIRAL_EFT_INPUT}:/home/chiraleft/input" \
    $CHIRAL_EFT_DOCKER_IMAGE_NAME:$CHIRAL_EFT_DOCKER_IMAGE_TAG python3 create_config.py --iso_start=0.0 --iso_end=1.0 --iso_step=0.05 --include_mean_field=0


docker run -it --rm \
    -v "${CHIRAL_EFT_DATA}:/home/chiraleft/data" \
    -v "${CHIRAL_EFT_INPUT}:/home/chiraleft/input" \
    -v "${CHIRAL_EFT_OUTPUT}:/home/chiraleft/output" \
    $CHIRAL_EFT_DOCKER_IMAGE_NAME:$CHIRAL_EFT_DOCKER_IMAGE_TAG make run


docker run -it --rm \
    -v "${CHIRAL_EFT_INPUT}:/home/chiraleft/input" \
    -v "${CHIRAL_EFT_OUTPUT}:/home/chiraleft/output" \
    $CHIRAL_EFT_DOCKER_IMAGE_NAME:$CHIRAL_EFT_DOCKER_IMAGE_TAG python3 clean_output.py


# #-----------------------------------------------------------------------------#
# # lepton-module execution
# #-----------------------------------------------------------------------------#

# Create test directories
mkdir -p "${LEPTON_INPUT}"
mkdir -p "${LEPTON_OUTPUT}"

# # Copy the output of CHIRAL_EFT into the input of lepton-module
cp ${CHIRAL_EFT_OUTPUT}/output_lepton.csv ${LEPTON_INPUT}/chiraleft_grid.csv



# Create config file inside container, this could be done outside the container too
docker run -it --rm \
    -v "${LEPTON_INPUT}:/home/lepton/input" \
    -v "${LEPTON_OUTPUT}:/home/lepton/output" \
    $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_generator.py \
                          --eos_input_file="chiraleft_grid.csv" \
                          --use_beta_equilibrium=true \
                          --use_charge_neutrality=false \
                          --use_fixed_lepton_fraction=false \
                          --use_electron=true \
                          --use_muon=true \
                          --use_tau=false \
                          --use_electron_neutrino=false \
                          --use_muon_neutrino=false \
                          --use_tau_neutrino=false \
                          --use_lepton_eos=false \
                          --use_temperature=true \
                          --use_magnetic_field=false \
                          --B_value=0 \
                          --use_anomalous_magnetic_moment=false \
                          --verbose=2

# Run the Docker container (validation, run lepton)
docker run -it --rm \
  -v "${LEPTON_INPUT}:/home/lepton/input" \
  -v "${LEPTON_OUTPUT}:/home/lepton/output" \
    $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG python3 yaml_validator.py

docker run -it --rm \
  -v "${LEPTON_INPUT}:/home/lepton/input" \
  -v "${LEPTON_OUTPUT}:/home/lepton/output" \
    $LEPTON_DOCKER_IMAGE_NAME:$LEPTON_DOCKER_IMAGE_TAG lepton

