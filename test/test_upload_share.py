import sys
import logging
import time
from typing import Dict
from calculation_engine_api import CalculationEngineApi, purge_all_user_data, get_staff_apis, purge_and_create_users

logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
logger = logging.getLogger(__name__)


def test_shared_upload(apis: Dict[str, CalculationEngineApi]) -> None:
    usernames = [username for username in apis]
    username = usernames[0]
    print(f'\nActing as user "{username}":\n')

    print(f'\nUpload the Lepton input EoS for "{username}":\n')
    upload_response = apis[username].upload_file(
        file_path='run/examples/eos.csv',
        public=False,
        upload_path='/lepton/eos.csv',
        description='Lepton example input EoS from v0.8.5',
    )
    upload_id = upload_response['uuid']
    print(f'\nSet uploaded file to public for "{username}":\n')
    apis[username].upload_file_update(uuid=upload_id, public=True)
    print(f'\nList uploaded files for "{username}":\n')
    apis[username].upload_list()

    def run_workflow(username):
        print(f'\nRun the singlet workflow for the test module for "{username}" using the Lepton EoS as input file shared by "{usernames[0]}":\n')
        description = f'Use the uploaded file owned by "{usernames[0]}"'
        request_config = {
            "workflow": {
                "name": "singlet",
                # "description": ,
                "config": {
                    "name": "template_module_2",
                    "config": {'ignore': 'me'},
                    "inputs": {
                        "eos": {
                            "type": "upload",
                            "uuid": upload_id
                        }
                    }
                }
            }
        }
        job_response = apis[username].job_create(name="use-uploaded-file", description=description, config=request_config)
        job_id = job_response['uuid']
        print(f'\nList all jobs for "{username}":\n')
        apis[username].job_list()
        print(f'\nStatus of current job for "{username}":\n')
        count = 0
        while count < 5 and job_response['status'] not in ['SUCCESS', 'FAILURE']:
            job_response = apis[username].job_list(uuid=job_id)
            count += 1
            time.sleep(2)
        print(f'\nList all jobs for "{username}":\n')
        apis[username].job_list()

    # The workflow should succeed because the upload is public
    username = usernames[1]
    print(f'\nActing as user "{username}":\n')
    run_workflow(username)

    # Switch to the uploaded file owner to switch it to private
    username = usernames[0]
    print(f'\nActing as user "{username}":\n')
    print(f'\nSet uploaded file to private for "{username}":\n')
    apis[username].upload_file_update(uuid=upload_id, public=False)

    # The workflow should fail because the upload is private
    username = usernames[1]
    print(f'\nActing as user "{username}":\n')
    run_workflow(username)


if __name__ == "__main__":
    staff_apis = get_staff_apis()
    logger.debug(staff_apis)
    purge_all_user_data(staff_apis)
    test_shared_upload(apis=staff_apis)
