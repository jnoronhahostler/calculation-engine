import sys
import logging
import time
from calculation_engine_api import get_staff_apis, get_admin_api, purge_all_user_data, purge_and_create_users

logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
logger = logging.getLogger(__name__)


def singlet_test(api):
    print('\nDelete all jobs:\n')
    api.job_delete_all()
    print('\nList all jobs:\n')
    api.job_list()
    print('\nList current uploaded files:\n')
    uploads = api.upload_list()
    print('\nDelete all uploaded files:\n')
    for upload in uploads['results']:
        api.delete_uploaded_file(uuid=upload['uuid'])

    print('\nUpload the Lepton input EoS:\n')
    upload_response = api.upload_file(
        file_path='run/examples/eos.csv',
        public=False,
        upload_path='/lepton/eos.csv',
        description='Lepton example input EoS from v0.8.5',
    )
    upload_id = upload_response['uuid']
    print('\nList uploaded files:\n')
    uploads = api.upload_list()
    print('\nSet uploaded file to public:\n')
    api.upload_file_update(uuid=upload_id, public=True)
    print('\nList uploaded files:\n')
    uploads = api.upload_list()
    print('\nRun the singlet workflow for the test module using the Lepton EoS as input file:\n')
    request_config = {
        "workflow": {
            "name": "singlet",
            "config": {
                "name": "template_module_2",
                "config": {'ignore': 'me'},
                "inputs": {
                    "eos": {
                        "type": "upload",
                        "uuid": upload_id
                    }
                }
            }
        }
    }
    job_response = api.job_create(name=f"use-uploaded-file", config=request_config)
    job_id = job_response['uuid']
    print('\nList all jobs:\n')
    api.job_list()
    print('\nStatus of current job:\n')
    count = 0
    while count < 5 and job_response['status'] not in ['SUCCESS', 'FAILURE']:
        job_response = api.job_list(uuid=job_id)
        count += 1
        time.sleep(2)
    print('\nList all saved jobs:\n')
    api.list_saved_jobs()
    if job_response['status'] == "SUCCESS":
        print(f'Saving job {job_id}:')
        api.save_job(job_id=job_id, public=False)
        print('\nList all saved jobs:\n')
        api.list_saved_jobs()
        print(f'Setting saved job {job_id} to public:')
        api.update_saved_job(job_id=job_id, public=True)
        print('\nList all saved jobs:\n')
        api.list_saved_jobs()
        # print(f'Deleting saved job {job_id}:')
        # api.delete_saved_job(job_id=job_id)
        # print('\nList all saved jobs:\n')
        # api.list_saved_jobs()

        # Run a new workflow using a saved job file
        print('\nRunning new workflow with previous job output file "/template_module_2/output/status.yaml":\n')
        saved_job_id = job_id
        request_config = {
            "workflow": {
                "name": "singlet",
                "config": {
                    "name": "template_module_2",
                    "config": {'ignore': 'me again'},
                    "inputs": {
                        "eos": {
                            "type": "job",
                            "uuid": saved_job_id,
                            "path": "/template_module_2/output/status.yaml"
                        }
                    }
                }
            }
        }
        job_response = api.job_create(name="use-saved-output", config=request_config)
        job_id = job_response['uuid']
        print('\nStatus of current job:\n')
        count = 0
        while count < 5 and job_response['status'] not in ['SUCCESS', 'FAILURE']:
            job_response = api.job_list(uuid=job_id)
            count += 1
            time.sleep(2)
        print('\nList all jobs:\n')
        api.job_list()
        # Run another workflow using a saved job file but specify invalid path
        print('\nRunning another workflow with original job output but specify invalid path:\n')
        request_config = {
            "workflow": {
                "name": "singlet",
                "config": {
                    "name": "template_module_2",
                    "config": {'ignore': 'me a third time'},
                    "inputs": {
                        "eos": {
                            "type": "job",
                            "uuid": saved_job_id,
                            "path": "FOO"
                        }
                    }
                }
            }
        }
        job_response = api.job_create(name="specify-invalid-path", config=request_config)
        job_id = job_response['uuid']
        print('\nStatus of current job:\n')
        count = 0
        while count < 5 and job_response['status'] not in ['SUCCESS', 'FAILURE']:
            job_response = api.job_list(uuid=job_id)
            count += 1
            time.sleep(2)
        print('\nList all jobs:\n')
        api.job_list()
        # Run another workflow using a saved job that does not exist
        print('\nRunning another workflow with a saved job that does not exist:\n')
        request_config = {
            "workflow": {
                "name": "singlet",
                "config": {
                    "name": "template_module_2",
                    "config": {'ignore': 'me a third time'},
                    "inputs": {
                        "eos": {
                            "type": "job",
                            "uuid": job_id,
                            "path": "/template_module_2/output/status.yaml"
                        }
                    }
                }
            }
        }
        job_response = api.job_create(name="specify-invalid-saved-job", config=request_config)
        job_id = job_response['uuid']
        print('\nStatus of current job:\n')
        count = 0
        while count < 5 and job_response['status'] not in ['SUCCESS', 'FAILURE']:
            job_response = api.job_list(uuid=job_id)
            count += 1
            time.sleep(2)
        print('\nList all jobs:\n')
        api.job_list()
    else:
        print('Job failed.')
        print('\nList all jobs:\n')
        api.job_list()


if __name__ == "__main__":
    admin_api = get_admin_api()
    admin_api.list_users()
    print('\n\n')
    purge_and_create_users()
    staff_apis = get_staff_apis()
    purge_all_user_data(staff_apis)
    singlet_test(api=[staff_apis[user] for user in staff_apis][0])
