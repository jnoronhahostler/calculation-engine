import sys
import logging
import os
from pathlib import Path
import yaml
import subprocess
from calculation_engine_api import get_admin_api
from calculation_engine_api import purge_and_create_users
from calculation_engine_api import get_staff_apis
from calculation_engine_api import run_and_monitor_workflow
logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
logger = logging.getLogger(__name__)


def download_script(job_id):
    script = f'''
        ORIG_DIR=$(pwd) && \
        cd /tmp && wget -r -nH -np --content-disposition \
        http://localhost:4000/files/jobs/{job_id}/ ;
        cd "${{ORIG_DIR}}"
    '''
    with open('/tmp/download_script.sh', 'w') as fp:
        fp.write(script)
    proc = subprocess.Popen(['bash', '/tmp/download_script.sh'])
    try:
        outs, errs = proc.communicate(timeout=60)
    except subprocess.TimeoutExpired:
        proc.kill()
        outs, errs = proc.communicate()
    print(outs, errs)


if __name__ == "__main__":
    print('\nPurge and recreate user accounts:\n')
    admin_api = get_admin_api()
    purge_and_create_users()
    admin_api.list_users()
    workflow_files = [
        'process_workflow.yaml',
        'group_workflow.yaml',
        'chain_workflow.yaml',
    ]
    for workflow_file in workflow_files:
        with open(os.path.join(Path(__file__).resolve().parent, 'test_workflow_config', workflow_file)) as fp:
            wf_config = yaml.load(fp, Loader=yaml.SafeLoader)
        username = 'foo'
        api = get_staff_apis(username=username)[username]
        job_response = run_and_monitor_workflow(
            api=api, config=wf_config,
            loops=10, sleep=10)
        job_id = job_response['uuid']
        # Download and view all job output
        download_script(job_id)
