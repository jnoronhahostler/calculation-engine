import sys
import logging
import yaml
from calculation_engine_api import CalculationEngineApi
from calculation_engine_api import run_and_monitor_workflow
logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
logger = logging.getLogger(__name__)


def run(api, wf_config_yaml, wait=True):
    wf_config = yaml.safe_load(wf_config_yaml)

    logger.debug(wf_config)
    print('This is the config: ', wf_config)

    print('Running job...')
    job_response = run_and_monitor_workflow(
        api=api, config=wf_config,
        loops=10, sleep=10, wait=wait)

    job_id = job_response['uuid']
    if not wait:
        return job_id

    print('Job finished: ', job_response)

    job_list = api.job_list(uuid=job_id)

    # Retrieve files
    for file_info in job_list['files']:
        print('File: ', file_info['path'])
        api.download_job_file(
            job_id, file_info['path'], root_dir='/tmp/ce/jobs/')

    return job_id


def run_cmf(api, wait=True):

    wf_config_yaml = '''
processes:
  - name: cmf
    module: cmf_solver
    config:
      variables:
        chemical_optical_potentials:
          muB_begin: 1000.0
          muB_end: 1500.0
          muB_step: 50.0
          muQ_begin: -300.0
          muQ_end: 0.0
          muQ_step: 20.0
      options:
        use_Polyakov: true
        vector_potential: 4
components:
  - type: group
    name: run_cmf_test
    group:
      - cmf
'''

    run(api, wf_config_yaml, wait=wait)


def run_ceft(api, wait=True):

    wf_config_yaml = '''
processes:
  - name: chiral_eft_eos
    module: chiral_eft_solver
    config:
      run_name: 'test_ceft'
      output_options:
        output_precision: 2.0e-12
        include_output_flavor: false
      eos_grid:
        isospin_asymmetry_start: 0.0
        isospin_asymmetry_end: 1.0
        isospin_asymmetry_step: 0.2
      computational_parameters:
        chiraleft_eos:
          epsilon: 1.0e-12
          first_order_eos:
            jmax: 4
            p_intervals: 5
            p_points: 3
components:
  - type: group
    name: run_ceft
    group:
      - chiral_eft_eos
'''
    run(api, wf_config_yaml, wait=wait)


def run_lepton(api, wait=True):

    wf_config_yaml_lepton = '''
processes:
  - name: lepton-module
    module: lepton
    inputs:
      input_eos:
        type: upload
        uuid: b2932d84-f9fd-4e4e-8877-0def7961d471
        path: /opt/cmf_grid/cmf_grid.csv
    config:
      options:
        global:
          run_name: ''
          use_lepton_eos: true
          use_beta_equilibrium: false
        particles:
          use_muon: true
        lepton_eos_parameters:
          electron_cp_initial: 0.0
          electron_cp_final: 100.0
          electron_cp_step: 1.0
        derivatives: {}
        input: {}
        output: {}
components:
  - type: group
    name: run_lepton
    group:
      - lepton-module
'''
    run(api, wf_config_yaml_lepton)


def run_cmf_lepton(api, wait=True):

    wf_config_yaml = '''
processes:
  - name: cmf
    module: cmf_solver
    config:
      variables:
        chemical_optical_potentials:
          muB_begin: 1000.0
          muB_end: 1500.0
          muB_step: 20.0
          muQ_begin: -300.0
          muQ_end: 0.0
          muQ_step: 15.0
  - name: lepton-module
    module: lepton
    config:
      global:
        run_name: ''
        use_beta_equilibrium: true
        verbose: 2
      derivatives:
        relative_step_size: 3.0e-6
        precision: 3
      input: {}
      lepton_eos_parameters: {}
      output: {}
      particles: {}
    pipes:
      input_eos:
        label: CMF_for_Lepton_baryons_only
        module: cmf_solver
        process: cmf
components:
  - type: chain
    name: cmf-lepton
    sequence:
      - cmf
      - lepton-module
'''

    run(api, wf_config_yaml, wait=wait)


def run_ceft_lepton(api, wait=True):
    wf_config_yaml = '''
processes:
  - name: chiral_eft_eos
    module: chiral_eft_solver
    config:
      run_name: 'test_ceft'
      output_options:
        output_precision: 2.0e-12
        include_output_flavor: false
      eos_grid:
        isospin_asymmetry_start: 0.0
        isospin_asymmetry_end: 1.0
        isospin_asymmetry_step: 0.2
      computational_parameters:
        chiraleft_eos:
          epsilon: 1.0e-12
          first_order_eos:
            jmax: 4
            p_intervals: 5
            p_points: 3
  - name: lepton-module
    module: lepton
    config:
      global:
        run_name: ''
        use_beta_equilibrium: true
        verbose: 2
      derivatives:
        relative_step_size: 3.0e-6
        precision: 3
      input: {}
      lepton_eos_parameters: {}
      output: {}
      particles: {}
    pipes:
      input_eos:
        label: ChEFT_Output_Lepton
        module: chiral_eft_solver
        process: chiral_eft_eos
components:
  - type: chain
    name: ceft-lepton
    sequence:
      - chiral_eft_eos
      - lepton-module
'''

    run(api, wf_config_yaml, wait=wait)


if __name__ == "__main__":

    api = CalculationEngineApi()

    max_loops = 10
    loop_idx = 0
    while loop_idx < max_loops:
        run_cmf(api, wait=False)
        run_ceft(api, wait=False)
        run_cmf_lepton(api, wait=False)
        run_ceft_lepton(api, wait=False)
        loop_idx += 1
