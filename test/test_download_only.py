import sys
import logging
from calculation_engine_api import get_staff_apis
logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
logger = logging.getLogger(__name__)


if __name__ == "__main__":
    username = 'foo'
    api = get_staff_apis(username=username)[username]
    job_id = sys.argv[1]
    file_path = sys.argv[2]

    print(f'Downloading job file "{file_path}"...')
    api.download_job_file(job_id, file_path)
