import sys
import logging
from calculation_engine_api import get_staff_apis
logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
logger = logging.getLogger(__name__)


if __name__ == "__main__":
    # username = 'foo'
    # api = get_staff_apis(username=username)[username]
    apis = get_staff_apis()
    usernames = [username for username in apis]

    username = usernames[0]
    api = apis[username]
    print(f'\nActing as user "{username}":\n')

    print(f'\nList current uploaded files for "{username}":\n')
    uploads = api.upload_list()
    print(f'\nDelete all uploaded files for "{username}":\n')
    for upload in uploads['results']:
        api.delete_uploaded_file(uuid=upload['uuid'])
    
    print('\nUpload the generic run.py script as private:\n')
    upload_response = api.upload_file(
        file_path='test/run.py',
        upload_path='/generic/run.py',
        description='Generic module runtime script',
        public=False,
    )
    upload_id = upload_response['uuid']

    username = usernames[1]
    api = apis[username]
    print(f'\nActing as user "{username}":\n')

    print(f'''\nTry to download {usernames[0]}'s private uploaded file:\n''')
    api.download_uploaded_file(upload_id, root_dir=f'/tmp/ce/uploads/{username}')

    username = usernames[0]
    api = apis[username]
    print(f'\nActing as user "{username}":\n')

    print('\nSet uploaded file to public:\n')
    api.upload_file_update(uuid=upload_id, public=True)

    username = usernames[1]
    api = apis[username]
    print(f'\nActing as user "{username}":\n')

    print(f'''\nTry to download {usernames[0]}'s public uploaded file:\n''')
    api.download_uploaded_file(upload_id, root_dir=f'/tmp/ce/uploads/{username}')
