import sys
import logging
import os
from pathlib import Path
import requests
import yaml
from calculation_engine_api import get_staff_apis
from calculation_engine_api import run_and_monitor_workflow
logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
logger = logging.getLogger(__name__)


if __name__ == "__main__":
    username = 'foo'
    api = get_staff_apis(username=username)[username]

    print(f'\nList current uploaded files for "{username}":\n')
    uploads = api.upload_list()
    print(f'\nDelete all uploaded files for "{username}":\n')
    for upload in uploads['results']:
        api.delete_uploaded_file(uuid=upload['uuid'])
    print('\nUpload the generic run.py script:\n')
    upload_response = api.upload_file(
        file_path='test/run.py',
        public=True,
        upload_path='/generic/run.py',
        description='Generic module runtime script',
    )
    upload_id = upload_response['uuid']
    with open(os.path.join(Path(__file__).resolve().parent, 'test_workflow_config', 'process_workflow.yaml')) as fp:
        wf_config = yaml.load(fp, Loader=yaml.SafeLoader)
    wf_config['processes'][0]['inputs']['script']['uuid'] = upload_id
    logger.debug(f'wf_config: {wf_config}')

    job_response = run_and_monitor_workflow(
        api=api, config=wf_config,
        loops=10, sleep=10)
    job_id = job_response['uuid']
    for file_path in job_response['files']:
        print(f'Downloading job file "{file_path}"...')
        api.download_job_file(job_id, file_path)
